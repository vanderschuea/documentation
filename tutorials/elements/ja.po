msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2018-02-15 21:44+0100\n"
"PO-Revision-Date: 2012-03-20 19:09+0900\n"
"Last-Translator: Masato Hashimoto <cabezon.hashimoto@gmail.com>\n"
"Language-Team: Japanese <inkscape-translator@lists.sourceforge.net>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Japanese\n"

#: elements-f15.svg:448(format) elements-f14.svg:169(format)
#: elements-f13.svg:98(format) elements-f12.svg:439(format)
#: elements-f11.svg:44(format) elements-f10.svg:44(format)
#: elements-f09.svg:44(format) elements-f08.svg:202(format)
#: elements-f07.svg:87(format) elements-f06.svg:87(format)
#: elements-f05.svg:44(format) elements-f04.svg:45(format)
#: elements-f03.svg:44(format) elements-f02.svg:44(format)
#: elements-f01.svg:44(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: elements-f12.svg:669(tspan)
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Random Ant &amp; 4WD"

#: elements-f12.svg:676(tspan)
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "SVG  Image Created by Andrew Fitzsimon"

#: elements-f12.svg:680(tspan)
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Courtesy of Open Clip Art Library"

#: elements-f12.svg:684(tspan)
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr "http://www.openclipart.org/"

#: elements-f04.svg:77(tspan)
#, no-wrap
msgid "BIG"
msgstr "BIG"

#: elements-f04.svg:92(tspan)
#, no-wrap
msgid "small"
msgstr "small"

#: elements-f01.svg:84(tspan)
#, no-wrap
msgid "Elements"
msgstr "要素"

#: elements-f01.svg:98(tspan)
#, no-wrap
msgid "Principles"
msgstr "原則"

#: elements-f01.svg:111(tspan) tutorial-elements.xml:90(title)
#, no-wrap
msgid "Color"
msgstr "色"

#: elements-f01.svg:124(tspan) tutorial-elements.xml:27(title)
#, no-wrap
msgid "Line"
msgstr "線"

#: elements-f01.svg:137(tspan) tutorial-elements.xml:43(title)
#, no-wrap
msgid "Shape"
msgstr "シェイプ"

#: elements-f01.svg:150(tspan) tutorial-elements.xml:73(title)
#, no-wrap
msgid "Space"
msgstr "スペース"

#: elements-f01.svg:163(tspan) tutorial-elements.xml:107(title)
#, no-wrap
msgid "Texture"
msgstr "テクスチャ"

#: elements-f01.svg:176(tspan) tutorial-elements.xml:122(title)
#, no-wrap
msgid "Value"
msgstr "明度"

#: elements-f01.svg:189(tspan) tutorial-elements.xml:59(title)
#, no-wrap
msgid "Size"
msgstr "サイズ"

#: elements-f01.svg:202(tspan) tutorial-elements.xml:144(title)
#, no-wrap
msgid "Balance"
msgstr "バランス"

#: elements-f01.svg:215(tspan) tutorial-elements.xml:160(title)
#, no-wrap
msgid "Contrast"
msgstr "コントラスト"

#: elements-f01.svg:228(tspan) tutorial-elements.xml:173(title)
#, no-wrap
msgid "Emphasis"
msgstr "強調"

#: elements-f01.svg:241(tspan) tutorial-elements.xml:188(title)
#, no-wrap
msgid "Proportion"
msgstr "比率"

#: elements-f01.svg:254(tspan) tutorial-elements.xml:202(title)
#, no-wrap
msgid "Pattern"
msgstr "パターン"

#: elements-f01.svg:268(tspan) tutorial-elements.xml:216(title)
#, no-wrap
msgid "Gradation"
msgstr "グラデーション"

#: elements-f01.svg:282(tspan) tutorial-elements.xml:234(title)
#, no-wrap
msgid "Composition"
msgstr "構図"

#: elements-f01.svg:296(tspan)
#, no-wrap
msgid "Overview"
msgstr "概略"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:17(None)
#, fuzzy
msgid "@@image: 'elements-f01.svg'; md5=2beea807ea67f0cba28e932275c728f2"
msgstr "@@image: 'elements-f08.svg'; md5=74e6530be897b5325de69c847c56e2a6"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:36(None)
msgid "@@image: 'elements-f02.svg'; md5=177eb223201074a08b532de7017e504e"
msgstr "@@image: 'elements-f02.svg'; md5=177eb223201074a08b532de7017e504e"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:52(None)
msgid "@@image: 'elements-f03.svg'; md5=e65e9b657139ab4414d9ffbdbc88073d"
msgstr "@@image: 'elements-f03.svg'; md5=e65e9b657139ab4414d9ffbdbc88073d"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:66(None)
#, fuzzy
msgid "@@image: 'elements-f04.svg'; md5=fa422233c426ffa9b6b651cac13af257"
msgstr "@@image: 'elements-f04.svg'; md5=6cb6880d423c6db3b412c81b78997b4b"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:83(None)
msgid "@@image: 'elements-f05.svg'; md5=dd24ff57715416cdf78879607fc7d3f2"
msgstr "@@image: 'elements-f05.svg'; md5=dd24ff57715416cdf78879607fc7d3f2"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:100(None)
msgid "@@image: 'elements-f06.svg'; md5=448791cf633cd763341c5b7961051737"
msgstr "@@image: 'elements-f06.svg'; md5=448791cf633cd763341c5b7961051737"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:115(None)
msgid "@@image: 'elements-f07.svg'; md5=adc006799c4a19b2dcc6f468f4568a21"
msgstr "@@image: 'elements-f07.svg'; md5=adc006799c4a19b2dcc6f468f4568a21"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:131(None)
msgid "@@image: 'elements-f08.svg'; md5=74e6530be897b5325de69c847c56e2a6"
msgstr "@@image: 'elements-f08.svg'; md5=74e6530be897b5325de69c847c56e2a6"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:153(None)
msgid "@@image: 'elements-f09.svg'; md5=65af76c61bafce75791ea8431d19305a"
msgstr "@@image: 'elements-f09.svg'; md5=65af76c61bafce75791ea8431d19305a"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:166(None)
msgid "@@image: 'elements-f10.svg'; md5=0d342d21e272b4b56c7554bfa5ccdb31"
msgstr "@@image: 'elements-f10.svg'; md5=0d342d21e272b4b56c7554bfa5ccdb31"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:181(None)
msgid "@@image: 'elements-f11.svg'; md5=ed150e615a140cbd267c9af0577bbb9d"
msgstr "@@image: 'elements-f11.svg'; md5=ed150e615a140cbd267c9af0577bbb9d"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:195(None)
#, fuzzy
msgid "@@image: 'elements-f12.svg'; md5=d8429af5f25f6ec9c2429d08da2573e5"
msgstr "@@image: 'elements-f01.svg'; md5=a630c16801fc6b19eaa1cef54b06f444"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:209(None)
msgid "@@image: 'elements-f13.svg'; md5=8e91b9a906224c0324c6d1883a1fd02d"
msgstr "@@image: 'elements-f13.svg'; md5=8e91b9a906224c0324c6d1883a1fd02d"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:225(None)
msgid "@@image: 'elements-f14.svg'; md5=c16afd57f035cbdd5b948b57ade618e7"
msgstr "@@image: 'elements-f14.svg'; md5=c16afd57f035cbdd5b948b57ade618e7"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:238(None)
msgid "@@image: 'elements-f15.svg'; md5=4fd16352ff10b47f931d7b82de6d59ae"
msgstr "@@image: 'elements-f15.svg'; md5=4fd16352ff10b47f931d7b82de6d59ae"

#: tutorial-elements.xml:4(title)
#, fuzzy
msgid "Elements of design"
msgstr "デザインの要素"

#: tutorial-elements.xml:7(para)
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please add, "
"subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"このチュートリアルは、芸術作品の制作において用いられるいろいろな特性を理解する"
"ために、通常芸術家を目指す学生が初期に教えられるデザインの要素と原則について紹"
"介します。"

#: tutorial-elements.xml:23(title)
msgid "Elements of Design"
msgstr "デザインの要素"

#: tutorial-elements.xml:24(para)
msgid "The following elements are the building blocks of design."
msgstr "以下がデザインを構成する要素になります。"

#: tutorial-elements.xml:28(para)
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"線は長さと方向の記号として定義され、面を横切る点によって作成されます。線はその"
"長さ、幅、方向、曲率、および色を変化させることができます。線は二次元 (紙にペン"
"で書かれた線)、または暗に三次元になり得ます。"

#: tutorial-elements.xml:44(para)
msgid ""
"A flat figure, shape is created when actual or implied lines meet to surround "
"a space. A change in color or shading can define a shape. Shapes can be "
"divided into several types: geometric (square, triangle, circle) and organic "
"(irregular in outline)."
msgstr ""
"平面図形のシェイプは、空間を囲む実際の、または暗示的な線の接続により作成されま"
"す。色や濃淡の変動でシェイプを定義できます。シェイプにはいくつかのタイプ、すな"
"わち幾何学図形 (四角形、三角形、円) と非整形のアウトラインにわけられます。"

#: tutorial-elements.xml:60(para)
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"これはオブジェクト、線、あるいはシェイプのバランスの変化についての言及です。現"
"実あるいは想像のオブジェクトにはサイズの変動があります。"

#: tutorial-elements.xml:74(para)
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. Space "
"is often called three-dimensional or two- dimensional. Positive space is "
"filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"スペースはオブジェクトの間、周囲、上、下、あるいはその中にある、空の、あるいは"
"開けた領域のことです。シェイプとフォームはそれらの中および周囲にあるスペースに"
"より作成されます。スペースはしばしば三次元または二次元と見なされます。正のス"
"ペースはシェイプやフォームで塗りつぶされ、負のスペースはシェイプやフォームに囲"
"まれています。"

#: tutorial-elements.xml:91(para)
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"色は、面で反射される光の波長によるその特徴です。色は 3 つの属性、すなわち色相 "
"(色の別称、赤や黄色のような名前で現される)、明度 (明るさや暗さ)、彩度 (鮮やか"
"さ) を持ちます。"

#: tutorial-elements.xml:108(para)
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"テクスチャは表面の特徴 (実際のテクスチャ) あるいはそのようみ見せる (暗黙のテク"
"スチャ) 手段です。テクスチャは粗い (rough)、滑らか (silky)、またはぶつぶつ "
"(pebbly) などの言葉で説明されます。"

#: tutorial-elements.xml:123(para)
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"明度はその見た目の明るさや暗さの事です。明度の変更はその色に白や黒を加えること"
"で実現できます。明暗法は構図内で著しく対照的な明るさと暗さを使用して描画しま"
"す。"

#: tutorial-elements.xml:140(title)
msgid "Principles of Design"
msgstr "デザインの原則"

#: tutorial-elements.xml:141(para)
msgid "The principles use the elements of design to create a composition."
msgstr "原則は、デザインの要素を用いて構図を作成します。"

#: tutorial-elements.xml:145(para)
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be used "
"in creating a balance in a composition."
msgstr ""
"バランスはシェイプ、フォーム、明度、色、その他と同じく視覚的な印象です。バラン"
"スは対称あるいは均等なバランスや、非対称および均一でないバランスをとり得ます。"
"オブジェクト、明度、色、テクスチャ、シェイプ、フォーム、その他は構図内でバラン"
"スを作成します。"

#: tutorial-elements.xml:161(para)
msgid "Contrast is the juxtaposition of opposing elements"
msgstr "コントラストは、相対する要素を並置する事です。"

#: tutorial-elements.xml:174(para)
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"強調は、アートワークの一部を目立たせ、注意を引くのに用いられます。興味の中心あ"
"るいは焦点は、まず最初に目に止まるように描かれた場所になります。"

#: tutorial-elements.xml:189(para)
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"比率は、サイズ、位置、あるいは別のものと比較したあるものの量を表します。"

#: tutorial-elements.xml:203(para)
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"パターンは、要素 (線、シェイプ、または色) の何度も繰り返すことによって作成され"
"ます。"

#: tutorial-elements.xml:217(para)
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"サイズおよび方向のグラデーションは線形遠近法を実現します。暖色から寒色や暗色か"
"ら明色のグラデーションは空気遠近法を実現します。グラデーションはシェイプに影響"
"を与え、動きを加えます。暗色から明色へのグラデーションは、シェイプに視覚的な動"
"きを与えます。"

#: tutorial-elements.xml:235(para)
msgid "The combining of distinct elements to form a whole."
msgstr "個別の要素すべてを一体化します。"

#: tutorial-elements.xml:245(title)
msgid "Bibliography"
msgstr "参考文献"

#: tutorial-elements.xml:246(para)
msgid "This is a partial bibliography used to build this document."
msgstr "以下はこの文書を作成するにあたり用いた参考文献の一部です。"

#: tutorial-elements.xml:249(ulink)
msgid "http://www.makart.com/resources/artclass/EPlist.html"
msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#: tutorial-elements.xml:252(ulink)
msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#: tutorial-elements.xml:255(ulink)
msgid "http://www.johnlovett.com/test.htm"
msgstr "http://www.johnlovett.com/test.htm"

#: tutorial-elements.xml:259(ulink)
msgid "http://digital-web.com/articles/elements_of_design/"
msgstr "http://digital-web.com/articles/elements_of_design/"

#: tutorial-elements.xml:263(ulink)
msgid "http://digital-web.com/articles/principles_of_design/"
msgstr "http://digital-web.com/articles/principles_of_design/"

#: tutorial-elements.xml:268(para)
#, fuzzy
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url="
"\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. "
"Also, thanks to the Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics "
"people have submitted to that project."
msgstr ""
"このチュートリアルを執筆するにあたり、私 (<ulink url=\"http://www.rejon.org/"
"\">http://www.rejon.org/</ulink>) を助けてくれた Linda Kim (<ulink url="
"\"http://www.redlucite.org\">http://www.redlucite.org</ulink>) に心より感謝し"
"ます。また、Open Clip Art Library (<ulink url=\"http://www.openclipart.org/"
"\">http://www.openclipart.org/</ulink>) およびそのプロジェクトに参加したグラ"
"フィッカーの皆様にも感謝します。"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-elements.xml:0(None)
msgid "translator-credits"
msgstr "Masato Hashimoto <cabezon.hashimoto@gmail.com>, 2009-2012."

#~ msgid "@@image: 'elements-f01.svg'; md5=a630c16801fc6b19eaa1cef54b06f444"
#~ msgstr "@@image: 'elements-f01.svg'; md5=a630c16801fc6b19eaa1cef54b06f444"

#~ msgid "@@image: 'elements-f12.svg'; md5=b1d423aad90abe6214114e639cb78ac7"
#~ msgstr "@@image: 'elements-f12.svg'; md5=b1d423aad90abe6214114e639cb78ac7"

#~ msgid "http://sanford-artedventures.com/study/study.html"
#~ msgstr "http://sanford-artedventures.com/study/study.html"

#~ msgid "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
#~ msgstr "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
