# Dutch translations for the calligraphy tutorial of Inkscape.
# This file is distributed under the same license as the Inkscape package.
#
# Kris De Gussem <kris.DeGussem@gmail.com>, 2010-2011.
#
# *** Stuur een mailtje naar <kris.DeGussem@gmail.com>
# *** voordat je met dit bestand aan de slag gaat,
# *** om dubbel werk en stijlbreuken te voorkomen.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2018-02-15 21:41+0100\n"
"PO-Revision-Date: 2011-07-15 23:30+0100\n"
"Last-Translator: Kris De Gussem <Kris.DeGussem@gmail.com>\n"
"Language-Team: Dutch\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: calligraphy-f10.svg:45(format) calligraphy-f09.svg:44(format)
#: calligraphy-f08.svg:44(format) calligraphy-f07.svg:42(format)
#: calligraphy-f06.svg:59(format) calligraphy-f05.svg:45(format)
#: calligraphy-f04.svg:74(format) calligraphy-f03.svg:44(format)
#: calligraphy-f02.svg:45(format) calligraphy-f01.svg:45(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: calligraphy-f10.svg:107(tspan)
#, no-wrap
msgid "Uncial hand"
msgstr "Unciaal"

#: calligraphy-f10.svg:119(tspan)
#, no-wrap
msgid "Carolingian hand"
msgstr "Karolingisch"

#: calligraphy-f10.svg:131(tspan)
#, no-wrap
msgid "Gothic hand"
msgstr "Gotisch"

#: calligraphy-f10.svg:143(tspan)
#, no-wrap
msgid "Bâtarde hand"
msgstr "Bastarda"

#: calligraphy-f10.svg:167(tspan)
#, no-wrap
msgid "Flourished Italic hand"
msgstr "Vloeiend cursief"

#: calligraphy-f07.svg:65(tspan)
#, no-wrap
msgid "slow"
msgstr "traag"

#: calligraphy-f07.svg:75(tspan)
#, no-wrap
msgid "medium"
msgstr "gemiddeld"

#: calligraphy-f07.svg:85(tspan)
#, no-wrap
msgid "fast"
msgstr "snel"

#: calligraphy-f07.svg:169(tspan)
#, no-wrap
msgid "tremor = 0"
msgstr "beving = 0"

#: calligraphy-f07.svg:180(tspan)
#, no-wrap
msgid "tremor = 10"
msgstr "beving = 10"

#: calligraphy-f07.svg:191(tspan)
#, no-wrap
msgid "tremor = 30"
msgstr "beving = 30"

#: calligraphy-f07.svg:202(tspan)
#, no-wrap
msgid "tremor = 50"
msgstr "beving = 50"

#: calligraphy-f07.svg:213(tspan)
#, no-wrap
msgid "tremor = 70"
msgstr "beving = 70"

#: calligraphy-f07.svg:224(tspan)
#, no-wrap
msgid "tremor = 90"
msgstr "beving = 90"

#: calligraphy-f07.svg:235(tspan)
#, no-wrap
msgid "tremor = 20"
msgstr "beving = 20"

#: calligraphy-f07.svg:246(tspan)
#, no-wrap
msgid "tremor = 40"
msgstr "beving = 40"

#: calligraphy-f07.svg:257(tspan)
#, no-wrap
msgid "tremor = 60"
msgstr "beving = 60"

#: calligraphy-f07.svg:268(tspan)
#, no-wrap
msgid "tremor = 80"
msgstr "beving = 80"

#: calligraphy-f07.svg:279(tspan)
#, no-wrap
msgid "tremor = 100"
msgstr "beving = 100"

#: calligraphy-f06.svg:80(tspan) calligraphy-f06.svg:96(tspan)
#: calligraphy-f06.svg:112(tspan) calligraphy-f05.svg:66(tspan)
#, no-wrap
msgid "angle = 30"
msgstr "hoek = 30"

#: calligraphy-f06.svg:84(tspan)
#, no-wrap
msgid "fixation = 100"
msgstr "fixatie = 100"

#: calligraphy-f06.svg:100(tspan)
#, no-wrap
msgid "fixation = 80"
msgstr "fixatie = 80"

#: calligraphy-f06.svg:116(tspan)
#, no-wrap
msgid "fixation = 0"
msgstr "fixatie = 0"

#: calligraphy-f05.svg:77(tspan)
#, no-wrap
msgid "angle = 60"
msgstr "hoek = 60"

#: calligraphy-f05.svg:88(tspan)
#, no-wrap
msgid "angle = 90"
msgstr "hoek = 90"

#: calligraphy-f05.svg:99(tspan) calligraphy-f04.svg:132(tspan)
#, no-wrap
msgid "angle = 0"
msgstr "hoek = 0"

#: calligraphy-f05.svg:110(tspan)
#, no-wrap
msgid "angle = 15"
msgstr "hoek = 15"

#: calligraphy-f05.svg:121(tspan)
#, no-wrap
msgid "angle = -45"
msgstr "hoek = -45"

#: calligraphy-f04.svg:110(tspan)
#, no-wrap
msgid "angle = 90 deg"
msgstr "hoek = 90 gr"

#: calligraphy-f04.svg:121(tspan)
#, no-wrap
msgid "angle = 30 (default)"
msgstr "hoek = 30 (standaard)"

#: calligraphy-f04.svg:143(tspan)
#, no-wrap
msgid "angle = -90 deg"
msgstr "hoek = -90 gr"

#: calligraphy-f02.svg:66(tspan)
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "versmalling = 0 (uniforme breedte) "

#: calligraphy-f02.svg:77(tspan)
#, no-wrap
msgid "thinning = 10"
msgstr "versmalling = 10"

#: calligraphy-f02.svg:88(tspan)
#, no-wrap
msgid "thinning = 40"
msgstr "versmalling = 40"

#: calligraphy-f02.svg:99(tspan)
#, no-wrap
msgid "thinning = -20"
msgstr "versmalling = -20"

#: calligraphy-f02.svg:110(tspan)
#, no-wrap
msgid "thinning = -60"
msgstr "versmalling = -60"

#: calligraphy-f01.svg:66(tspan)
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "breedte=1, stijgend...                       nadert 47, dalend ...                                   terug naar 0"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:144(None)
msgid "@@image: 'calligraphy-f01.svg'; md5=f27939e8c74b022d1d270f16b17c1484"
msgstr "@@image: 'calligraphy-f01.svg'; md5=0a1fb79de82dc95c049c3a0c34346ac2"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:160(None)
msgid "@@image: 'calligraphy-f02.svg'; md5=0caeffd9b92114393aa383dbc325421f"
msgstr "@@image: 'calligraphy-f01.svg'; md5=0a1fb79de82dc95c049c3a0c34346ac2"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:172(None)
msgid "@@image: 'calligraphy-f03.svg'; md5=4650561753cc124def3629d1cd61836a"
msgstr "@@image: 'calligraphy-f03.svg'; md5=4650561753cc124def3629d1cd61836a"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:192(None)
msgid "@@image: 'calligraphy-f04.svg'; md5=576e94131ef9f3a278a179ec4146f39e"
msgstr "@@image: 'calligraphy-f03.svg'; md5=4650561753cc124def3629d1cd61836a"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:209(None)
msgid "@@image: 'calligraphy-f05.svg'; md5=0bedc63ef08031de2b8de8bd2757665a"
msgstr "@@image: 'calligraphy-f05.svg'; md5=02b5d45808e65e3c6152d82c175bf9df"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:230(None)
msgid "@@image: 'calligraphy-f06.svg'; md5=0d0ca38000f0a14ba641cb8c22ae93f4"
msgstr "@@image: 'calligraphy-f02.svg'; md5=537004565d832c4a5c57e1f163c2b46b"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:255(None)
msgid "@@image: 'calligraphy-f07.svg'; md5=37055cfeff519d5a6ef7f0577cd53060"
msgstr "@@image: 'calligraphy-f01.svg'; md5=0a1fb79de82dc95c049c3a0c34346ac2"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:304(None)
msgid "@@image: 'calligraphy-f08.svg'; md5=93bbba3eaf9bfd44ad859e76d11802d1"
msgstr "@@image: 'calligraphy-f08.svg'; md5=93bbba3eaf9bfd44ad859e76d11802d1"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:322(None)
msgid "@@image: 'calligraphy-f09.svg'; md5=b3812b1642a8927f25557a33011dfcf2"
msgstr "@@image: 'calligraphy-f09.svg'; md5=b3812b1642a8927f25557a33011dfcf2"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-calligraphy.xml:359(None)
msgid "@@image: 'calligraphy-f10.svg'; md5=f5de4610f6e4d340376ac847522e57b8"
msgstr "@@image: 'calligraphy-f06.svg'; md5=fc36d9da92c99a158ac2ec651a1b312b"

#: tutorial-calligraphy.xml:4(title)
msgid "Calligraphy"
msgstr "Kalligrafie"

#: tutorial-calligraphy.xml:5(author)
msgid "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
msgstr "Bulia Byak, buliabyak@users.sf.net en Josh Andler, scislac@users.sf.net"

#: tutorial-calligraphy.xml:9(para)
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Een van de fantastische gereedschappen die beschikbaar is in Inkscape is het "
"gereedschap kalligrafie. Deze handleiding helpt je op weg met de werking van "
"het gereedschap en demonstreert enkele basistechnieken in de kalligrafiekunst."

#: tutorial-calligraphy.xml:14(para)
msgid ""
"Use <keycap>Ctrl+arrows</keycap>, <keycap>mouse wheel</keycap>, or "
"<keycap>middle button drag</keycap> to scroll the page down. For basics of "
"object creation, selection, and transformation, see the Basic tutorial in "
"<command>Help &gt; Tutorials</command>."
msgstr ""
"Gebruik <keycap>Ctrl+Pijltjestoetsen</keycap>, <keycap>muiswiel</keycap> of "
"<keycap>middenmuisknop</keycap> om naar beneden te scrollen. Voor de basis "
"van objecten maken, selectie en transformatie, zie de Basishandleiding in "
"<command>Help &gt; Handleidingen</command>."

#: tutorial-calligraphy.xml:22(title)
msgid "History and Styles"
msgstr "Geschiedenis en stijlen"

#: tutorial-calligraphy.xml:24(para)
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, calligraphy "
"is the art of making beautiful or elegant handwriting. It may sound "
"intimidating, but with a little practice, anyone can master the basics of "
"this art."
msgstr ""
"De woordenboekdefinitie van <firstterm>kalligrafie</firstterm> is “mooi "
"schrijven” of “mooie en elegante schrijfkunst”. Per definitie is kalligrafie "
"de kunst van het maken van mooie of elegante handschriften. Het komt "
"misschien vreemd over, maar met een beetje oefening kan iedereen de basis van "
"deze kunst beheersen."

#: tutorial-calligraphy.xml:31(para)
msgid ""
"The earliest forms of calligraphy date back to cave-man paintings. Up until "
"roughly 1440 AD, before the printing press was around, calligraphy was the "
"way books and other publications were made. A scribe had to handwrite every "
"individual copy of every book or publication. The handwriting was done with a "
"quill and ink onto materials such as parchment or vellum. The lettering "
"styles used throughout the ages include Rustic, Carolingian, Blackletter, "
"etc. Perhaps the most common place where the average person will run across "
"calligraphy today is on wedding invitations."
msgstr ""
"De oudste vormen van kalligrafie gaan terug op de grottekeningen. Tot "
"ongeveer 1440 n.Chr., voor de boekdrukkunst was uitgevonden, was kalligrafie "
"de methode voor het maken van boeken en andere publicaties. Een "
"schriftgeleerde moest elke individuele kopie van elk boek of publicatie met "
"de hand schrijven. Schrijven gebeurde met een veer en inkt op materialen "
"zoals perkament of doek. De door de eeuwen gebruikte lettertypes omvatten het "
"Rustica, Karolingisch, Gotisch, etc. De meest waarschijnlijke gelegenheid "
"waarbij men heden ten dage kalligrafie tegenkomt is op huwelijksuitnodigingen."

#: tutorial-calligraphy.xml:41(para)
msgid "There are three main styles of calligraphy:"
msgstr "Er zijn drie hoofdstijlen van kalligrafie:"

#: tutorial-calligraphy.xml:46(para)
msgid "Western or Roman"
msgstr "Westers of Romeins"

#: tutorial-calligraphy.xml:49(para)
msgid "Arabic"
msgstr "Arabisch"

#: tutorial-calligraphy.xml:52(para)
msgid "Chinese or Oriental"
msgstr "Chinees of Oriëntaals"

#: tutorial-calligraphy.xml:57(para)
msgid ""
"This tutorial focuses mainly on Western calligraphy, as the other two styles "
"tend to use a brush (instead of a pen with nib), which is not how our "
"Calligraphy tool currently functions."
msgstr ""
"Deze handleiding focust in hoofdzaak op de Westerse kalligrafie, aangezien er "
"bij de andere twee stijlen vaak een borstel gebruikt wordt (in plaats van een "
"pen met punt), wat niet de wijze is waarop ons kalligrafiegereedschap nu "
"functioneert."

#: tutorial-calligraphy.xml:63(para)
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<command>Undo</command> command: If you make a mistake, the entire page is "
"not ruined. Inkscape's Calligraphy tool also enables some techniques which "
"would not be possible with a traditional pen-and-ink."
msgstr ""
"Een handig voordeel dat we hebben ten opzichte van de schriftkunst uit het "
"verleden is de opdracht <command>Ongedaan maken</command>: bij een fout is de "
"volledige pagina niet geruïneerd. Inkscape's kalligrafiegereedschap laat ook "
"enkele technieken toe die niet mogelijk zijn met het traditionele pen en "
"papier."

#: tutorial-calligraphy.xml:72(title)
msgid "Hardware"
msgstr "Hardware"

#: tutorial-calligraphy.xml:74(para)
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there will "
"be some difficulty producing fast sweeping strokes."
msgstr ""
"Je zal de beste resultaten verkrijgen indien je een <firstterm>tablet en pen</"
"firstterm> gebruikt (bv. Wacom). Dankzij de flexibiliteit van het gereedschap "
"kan zelfs diegene met alleen een muis vrij ingewikkelde kalligrafie maken, "
"alhoewel er moeilijkheden zullen zijn met het maken van snelle vegen."

#: tutorial-calligraphy.xml:81(para)
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Inkscape kan gebruik maken van de <firstterm>drukgevoeligheid</firstterm> en "
"<firstterm>hellingsgevoeligheid</firstterm> van een tabletpen dat deze "
"features ondersteunt. De gevoeligheidsfuncties zijn standaard uitgeschakeld, "
"omdat ze juist ingesteld moeten worden. Houdt ook in het achterhoofd dat "
"kalligrafie met veer of pen met punt ook niet gevoelig zijn voor druk in "
"tegenstelling met een borstel."

#: tutorial-calligraphy.xml:88(para)
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <firstterm>Input Devices...</firstterm> dialog through the "
"<emphasis>Edit</emphasis> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar buttons "
"for pressure and tilt. From now on, Inkscape will remember those settings on "
"startup."
msgstr ""
"Indien je een tablet hebt en de gevoeligheidsfunctionaliteit wil gebruiken, "
"moet je het toestel configureren. Deze configuratie is slechts éénmalig "
"nodig; de instellingen worden bewaard. Voor het inschakelen van de "
"ondersteuning moet het tablet ingeplugd zijn voor het starten van Inkscape en "
"ga dan naar <firstterm>Invoerapparaten...</firstterm> in het menu "
"<emphasis>Bestand</emphasis>. Met dit dialoogvenster kan je het "
"voorkeursinstellingen voor het toestel en tablet pen kiezen. Schakel "
"vervolgens over naar het kalligrafiegereedschap en verschakel de knoppen voor "
"druk en hoek in de knoppenbalk. Vanaf nu zal Inkscape deze instellingen "
"onthouden."

#: tutorial-calligraphy.xml:99(para)
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a mouse, "
"you'll probably want to zero this parameter."
msgstr ""
"De kalligrafiepen van Inkscape kan gevoelig zijn voor de <firstterm>snelheid</"
"firstterm> van de pennentrek (zie hieronder bij “Versmalling”). Bijgevolg, "
"indien je een muis gebruikt, is 0 wellicht de beste waarde voor deze "
"parameter."

#: tutorial-calligraphy.xml:107(title)
msgid "Calligraphy Tool Options"
msgstr "Opties kalligrafiegereedschap"

#: tutorial-calligraphy.xml:109(para)
msgid ""
"Switch to the Calligraphy tool by pressing <keycap>Ctrl+F6</keycap>, pressing "
"the <keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: Width &amp; Thinning; Angle "
"&amp; Fixation; Caps; Tremor, Wiggle &amp; Mass. There are also two buttons "
"to toggle tablet Pressure and Tilt sensitivity on and off (for drawing "
"tablets)."
msgstr ""
"Schakel over naar het kalligrafiegereedschap door te drukken op <keycap>Ctrl"
"+F6</keycap>, op <keycap>C</keycap>, of door te klikken op de knoppenbalk. Op "
"de gereedschapsdetailsbalk zijn er 8 opties zichtbaar: Breedte &amp; "
"Versmalling &amp; Hoek &amp; Oriëntatie &amp; Kapje &amp; Beving &amp; "
"Wegglijden &amp; Massa. Er zijn ook nog twee knoppen voor het inschakelen van "
"druk- en hoekgevoeligheid (voor tekentabletten)."

#: tutorial-calligraphy.xml:119(title)
msgid "Width &amp; Thinning"
msgstr "Breeedte &amp; versmalling"

#: tutorial-calligraphy.xml:121(para)
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr ""
"Deze twee opties bepalen de <firstterm>breedte</firstterm> van je pennentrek. "
"De breedte kan variëren tussen 1 en 100 en wordt (standaard) gemeten in "
"eenheden relatief ten opzichte van je bewerkingsvenster, onafhankelijk van de "
"zoom. Dit is logisch, omdat de natuurlijke “maat” in kalligrafie de omvang "
"van de handbewegingen is. Het is dus handig om de breedte van je penpunt in "
"constante verhouding met de grootte van je “tekenplank” te hebben en niet in "
"reële eenheden, die het afhankelijk van de zoom zouden maken. Dit gedrag is "
"echter optioneel. Je kan het veranderen indien je absolute eenheden van zoom "
"verkiest. Om naar deze modus over te schakelen, gebruik het selectievakje op "
"de voorkeurpagina van het gereedschap (open door de dubbelklikken op de "
"gereedschapsknop."

#: tutorial-calligraphy.xml:134(para)
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Aangezien de penbreedte vaak verandert wordt, kan je deze veranderen zonder "
"tussenkomst van de knoppenbalk met de pijltjestoetsen <keycap>links</keycap> "
"en <keycap>rechts</keycap> of met een tablet dat drukgevoeligheid "
"ondersteunt. Het leukste aan deze toetsen is dat ze werken terwijl je tekent. "
"Je kan dus de breedte van je pennentrek gradueel veranderen in het midden van "
"de lij:"

#: tutorial-calligraphy.xml:149(para)
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"De penbreedte kan ook afhankelijk zijn van de snelheid, bepaald door de "
"parameter <firstterm>Versmalling</firstterm>. Deze parameter kan waarden "
"aannemen van -100 tot 100. Nul betekent dat de breedte onafhankelijk is van "
"de snelheid, positieve waarden maakt snelle pennentrekken smaller, terwijl "
"negatieve waarden snelle pennentrekken breder maakt. De standaardwaarde van "
"10 zorgt voor middelmatige verdunning van snelle pennentrekken. Hier zijn "
"enekel voorbeelden getekent met breedte=20 en hoek=90:"

#: tutorial-calligraphy.xml:165(para)
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Voor de fun, stel breedte en verdunning beide in op 100 (maximum) en teken "
"met schokkerige bewegingen om rare natuurlijk neuron-achtige vormen te "
"krijgen:"

#: tutorial-calligraphy.xml:180(title)
msgid "Angle &amp; Fixation"
msgstr "Hoek &amp; fixatie"

#: tutorial-calligraphy.xml:182(para)
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the angle "
"parameter is greyed out and the angle is determined by the tilt of the pen."
msgstr ""
"Na de breedte is de <firstterm>hoek</firstterm> de meest belangrijke "
"kalligrafieparameter. Het is de hoek van je pen in graden, van 0 "
"(horizontaal) tot 90 (verticaal tegen de wijzers van de klok) of tot -90 "
"(verticaal met de wijzers van de klok). Onthou dat wanneer je "
"hoekgevoeligheid voor je tablet inschakelt, de parameter hoek grijs kleurt, "
"aangezien de hoek bepaalt wordt door de hoek van je pen."

#: tutorial-calligraphy.xml:197(para)
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands and "
"more experienced calligraphers will often vary the angle while drawing, and "
"Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Elke traditionele kalligrafiestijl heeft zijn eigen standaard penhoek. "
"Bijvoorbeeld, het Unciaal handschrift gebruikt een hoek van 25 graden. Bij "
"meer gecompliceerde handschriften en bij gevorderde kalligrafen zal de hoek "
"variëren tijdens het tekenen. Inkscape maakt dit mogelijk door het drukken op "
"de pijltjestoetsen <keycap>omhoog</keycap> en <keycap>omlaag</keycap> of met "
"een tablet dat hoekgevoeligheid ondersteunt. Voor kalligrafielessen voor "
"beginners werkt het constant van de hoek het beste. Hier zijn voorbeelden van "
"pennentrekken getekend met verschillende hoeken (fixatie=100):"

#: tutorial-calligraphy.xml:214(para)
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Zoals je kan zien, is de lijn het smalst wanneer deze parallel en het breedst "
"wanneer deze loodrecht met de oriëntatie getekend is. Positieve hoeken zijn "
"het meest natuurlijk en traditioneel voor rechtshandige kalligrafie."

#: tutorial-calligraphy.xml:220(para)
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"De mate van contrast tussen het smalste en breedste wordt bepaald door de "
"parameter <firstterm>fixatie</firstterm>. De waarde 100 betekent dat de hoek "
"altijd constant is zoals ingesteld bij Hoek. Het verlagen van de fixatie laat "
"de pen een beetje tegen de richting van de lijn draaien. Met fixatie=0 draait "
"de pen vrij zodat deze altijd loodrecht staat op de lijn en de hoek geen "
"effect meer heeft:"

#: tutorial-calligraphy.xml:235(para)
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke width "
"contrast (above left) are the features of antique serif typefaces, such as "
"Times or Bodoni (because these typefaces were historically an imitation of "
"fixed-pen calligraphy). Zero fixation and zero width contrast (above right), "
"on the other hand, suggest modern sans serif typefaces such as Helvetica."
msgstr ""
"Vanuit typografisch standpunt zijn maximum fixatie en bijgevolg maximum "
"lijnbreedtecontrast (boven links) de kenmerken van de oude serif lettertypen, "
"zoals Times of Bodoni, omdat deze lettertypen historisch een imitatie waren "
"van vaste-pen-kalligrafie. Nul fixatie en nul breedtecontrast (boven rechts) "
"aan de andere kant, suggereren moderne sans serif lettertypen zoals Helvetica."

#: tutorial-calligraphy.xml:245(title)
msgid "Tremor"
msgstr "Beving"

#: tutorial-calligraphy.xml:247(para)
#, fuzzy
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"<firstterm>Beving</firstterm> is bedoeld om een meer natuurlijk uitzicht te "
"geven aan de kalligrafie. De mate van beving is aanpasbaar in de "
"gereedschapsdetailsbalk tussen 0,0 en 1,0. Het beïnvloedt je lijnen zodat je "
"alles tussen lichte oneffenheden en wilde vlekken kan verkrijgen. Dit "
"vergroot het creatieve interval van het gereedschap significant."

#: tutorial-calligraphy.xml:262(title)
msgid "Wiggle &amp; Mass"
msgstr "Wegglijden &amp; Massa"

#: tutorial-calligraphy.xml:264(para)
msgid ""
"Unlike width and angle, these two last parameters define how the tool “feels” "
"rather than affect its visual output. So there won't be any illustrations in "
"this section; instead just try them yourself to get a better idea."
msgstr ""
"In tegenstelling tot breedte en hoek definiëren deze laatste twee parameters "
"eerder hoe het gereedschap “aanvoelt” dan dat ze de visuele output "
"beïnvloeden. Bijgevolg zijn er ook geen illustraties in deze sectie. Probeer "
"ze gewoon om er een beter idee van te krijgen."

#: tutorial-calligraphy.xml:270(para)
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"<firstterm>Wegglijden</firstterm> is de weerstand van het papier tegen de "
"beweging van de pen. Standaard staat deze op het minimum (0). Deze parameter "
"vergroten maakt het papier “slipperig”: indien de massa groot is, heeft de "
"pen de neiging om weg te glijden bij scherpe bochten; indien de massa nul is, "
"zorgt een hoge waarde voor wegglijden dat de pen wild wiebelt."

#: tutorial-calligraphy.xml:277(para)
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your mouse "
"pointer and the more it smoothes out sharp turns and quick jerks in your "
"stroke. By default this value is quite small (2) so that the tool is fast and "
"responsive, but you can increase mass to get slower and smoother pen."
msgstr ""
"In de natuurkunde is <firstterm>massa</firstterm> datgene wat traagheid "
"veroorzaakt. Hoe hoger de massa van het Inkscape kalligrafiegereedschap, hoe "
"meer het achterblijft op je muispointer en hoe meer het scherpe bochten en "
"snelle trekken in je lijn afvlakt. Standaard is deze waarde vrij klein (2) "
"zodat het gereedschap snel en responsief is, maar je kan de massa verhogen om "
"een tragere en effenere pen te krijgen."

#: tutorial-calligraphy.xml:288(title)
msgid "Calligraphy examples"
msgstr "Kalligrafievoorbeelden"

#: tutorial-calligraphy.xml:290(para)
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Nu dat je de basismogelijkheden van het gereedschap kent, kan je proberen om "
"enige echte kalligrafie te maken. Indien je nieuw in deze kunst bent, haal "
"een goed kalligrafieboek en bestudeer het met Inkscape. Deze sectie zal je "
"enkele eenvoudige voorbeelden tonen."

#: tutorial-calligraphy.xml:296(para)
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Om letter te maken moet je eerst en vooral een aantal linialen maken om je te "
"helpen. Indien je in cursief schrijft, voeg enkele schuine hulplijnen toe "
"tussen de twee linialen, bijvoorbeeld:"

#: tutorial-calligraphy.xml:309(para)
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Zoom vervolgens in zodat de hoogte tussen de linialen overeenkomt met je "
"meest natuurlijke range van handbewegingen, pas de breedte en hoek aan en "
"schrijven maar!"

#: tutorial-calligraphy.xml:314(para)
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round strokes, "
"slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"Wellicht is het eerste ding dat je als beginnende kalligraaf wil doen, de "
"basiselementen van letters oefenen — verticale en horizontale lijnen, ronde "
"lijnen en cursieve lijnen. Hier zijn enkele letteronderdelen van het Unciaal "
"handschrift:"

#: tutorial-calligraphy.xml:327(para)
msgid "Several useful tips:"
msgstr "Enkele bruikbare tips:"

#: tutorial-calligraphy.xml:332(para)
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll the "
"canvas (<keycap>Ctrl+arrow</keycap> keys) with your left hand after finishing "
"each letter."
msgstr ""
"Indien je hand confortabel zit op het tablet, verplaats het niet. Scroll in "
"plaats daarvan het canvas (<keycap>Ctrl+pijltjestoetsen</keycap>) met je "
"linkerhand na het beëindigen van elke letter."

#: tutorial-calligraphy.xml:337(para)
msgid ""
"If your last stroke is bad, just undo it (<keycap>Ctrl+Z</keycap>). However, "
"if its shape is good but the position or size are slightly off, it's better "
"to switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using mouse or keys), then press <keycap>Space</keycap> "
"again to return to Calligraphy tool."
msgstr ""
"Indien je laatste lijn slecht is, maak deze gewoon ongedaan (<keycap>Ctrl+Z</"
"keycap>). Echter, indien de vorm goed is, maar de positie of grootte is niet "
"perfect, is het beter om tijdelijk naar het selectiegereedschap over te "
"schakelen (<keycap>Spatie</keycap>) en verplaats/schaal/roteer zoals gewenst "
"(met de muis of het toetsenbord), druk dan opnieuw op <keycap>Spatie</keycap> "
"om naar het kalligrafiegereedschap terug te gaan."

#: tutorial-calligraphy.xml:345(para)
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Schakel na het beëindigen terug over naar het selectiegereedschap om "
"uniformiteit en letterspatiëring aan te passen. Overdrijf hier echter niet "
"mee: goede kalligrafie moet een wat onregelmatig uitzicht behouden. Weersta "
"de drang om letters en letteronderdelen te kopiëren; elke lijn moet origineel "
"zijn."

#: tutorial-calligraphy.xml:353(para)
msgid "And here are some complete lettering examples:"
msgstr "Hier zijn nog enkele lettervoorbeelden:"

#: tutorial-calligraphy.xml:369(title)
msgid "Conclusion"
msgstr "Conclusie"

#: tutorial-calligraphy.xml:371(para)
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with and "
"may be useful in real design. Enjoy!"
msgstr ""
"Kalligrafie is niet alleen fun. Het is een diep spirituele kunst die je zicht "
"op alles wat je doet en ziet, kan beïnvloeden. Inkscape's "
"kalligrafiegereedschap kan alleen dienen als een bescheiden introductie. En "
"toch is het leuk om mee te spelen en kan het bruikbaar zijn in echte "
"ontwerpen. Veel plezier!"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-calligraphy.xml:0(None)
msgid "translator-credits"
msgstr "Kris De Gussem <kris.DeGussem@gmail.com>, 2010-2011."

#, fuzzy
#~ msgid "@@image: 'calligraphy-f10.svg'; md5=d9b02c7d1ff70321c44e1e1281093253"
#~ msgstr "@@image: 'calligraphy-f10.svg'; md5=f5bab3f6024e2c07e57a166c7463ce4a"

#, fuzzy
#~ msgid "@@image: 'calligraphy-f04.svg'; md5=6f5e90597790d5c5c8e48557a74878e8"
#~ msgstr "@@image: 'calligraphy-f10.svg'; md5=f5bab3f6024e2c07e57a166c7463ce4a"

#, fuzzy
#~ msgid "@@image: 'calligraphy-f07.svg'; md5=b0d02ae9d1284efc86763d03bc74708b"
#~ msgstr "@@image: 'calligraphy-f07.svg'; md5=25ff9cd65254d662705472f4a571bf89"
