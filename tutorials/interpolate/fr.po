#
# Sylvain Chiron <chironsylvain@orange.fr>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Interpolate\n"
"POT-Creation-Date: 2018-02-15 21:47+0100\n"
"PO-Revision-Date: 2017-06-28 01:16+0200\n"
"Last-Translator: Sylvain Chiron <chironsylvain@orange.fr>\n"
"Language-Team: français <>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.2\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: interpolate-f20.svg:55(format) interpolate-f19.svg:52(format)
#: interpolate-f18.svg:45(format) interpolate-f17.svg:45(format)
#: interpolate-f16.svg:45(format) interpolate-f15.svg:45(format)
#: interpolate-f14.svg:45(format) interpolate-f13.svg:45(format)
#: interpolate-f12.svg:45(format) interpolate-f11.svg:52(format)
#: interpolate-f10.svg:45(format) interpolate-f09.svg:45(format)
#: interpolate-f08.svg:45(format) interpolate-f07.svg:45(format)
#: interpolate-f05.svg:45(format) interpolate-f04.svg:47(format)
#: interpolate-f03.svg:45(format) interpolate-f02.svg:47(format)
#: interpolate-f01.svg:45(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: interpolate-f11.svg:113(flowPara) interpolate-f04.svg:108(flowPara)
#: interpolate-f02.svg:117(flowPara)
#, no-wrap
msgid "Exponent: 0.0"
msgstr "Exposant : 0,0"

#: interpolate-f11.svg:114(flowPara) interpolate-f04.svg:109(flowPara)
#: interpolate-f02.svg:118(flowPara)
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr "Étapes d'interpolation : 6"

#: interpolate-f11.svg:115(flowPara) interpolate-f04.svg:110(flowPara)
#: interpolate-f02.svg:119(flowPara)
#, no-wrap
msgid "Interpolation Method: 2"
msgstr "Méthode d'interpolation : 2"

#: interpolate-f11.svg:116(flowPara) interpolate-f04.svg:111(flowPara)
#: interpolate-f02.svg:120(flowPara)
#, no-wrap
msgid "Duplicate Endpaths: unchecked"
msgstr "Dupliquer les extrémités : décoché"

#: interpolate-f11.svg:117(flowPara) interpolate-f04.svg:112(flowPara)
#: interpolate-f02.svg:121(flowPara)
#, no-wrap
msgid "Interpolate Style: unchecked"
msgstr "Interpoler le style : décoché"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:27(None)
msgid "@@image: 'interpolate-f01.svg'; md5=97881f947107640f7b8e333a949ff14c"
msgstr "@@image: 'interpolate-f01.svg'; md5=97881f947107640f7b8e333a949ff14c"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:36(None)
msgid "@@image: 'interpolate-f02.svg'; md5=d980264e812797c07f9190923bb211b5"
msgstr "@@image: 'interpolate-f02.svg'; md5=d980264e812797c07f9190923bb211b5"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:51(None)
msgid "@@image: 'interpolate-f03.svg'; md5=28c072ceeacdb2bdd7c2259ffbe599ad"
msgstr "@@image: 'interpolate-f03.svg'; md5=28c072ceeacdb2bdd7c2259ffbe599ad"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:60(None) tutorial-interpolate.xml:82(None)
msgid "@@image: 'interpolate-f04.svg'; md5=73d700c3c612f13f30dff2c93f935351"
msgstr "@@image: 'interpolate-f04.svg'; md5=73d700c3c612f13f30dff2c93f935351"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:73(None)
msgid "@@image: 'interpolate-f05.svg'; md5=4b4454eb3eb522658e217d20187e32e4"
msgstr "@@image: 'interpolate-f05.svg'; md5=4b4454eb3eb522658e217d20187e32e4"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:91(None)
msgid "@@image: 'interpolate-f07.svg'; md5=0c542eea7053a06ef83d5cc38b6cfd95"
msgstr "@@image: 'interpolate-f07.svg'; md5=0c542eea7053a06ef83d5cc38b6cfd95"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:98(None)
msgid "@@image: 'interpolate-f08.svg'; md5=e6cecf42fbd1025be356f25776f3ec8b"
msgstr "@@image: 'interpolate-f08.svg'; md5=e6cecf42fbd1025be356f25776f3ec8b"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:113(None)
msgid "@@image: 'interpolate-f09.svg'; md5=2c83c895aa047a9420a97089045065b4"
msgstr "@@image: 'interpolate-f09.svg'; md5=2c83c895aa047a9420a97089045065b4"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:122(None)
msgid "@@image: 'interpolate-f10.svg'; md5=4c6045fda8b91263faa9d0aa545a1786"
msgstr "@@image: 'interpolate-f10.svg'; md5=4c6045fda8b91263faa9d0aa545a1786"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:140(None)
msgid "@@image: 'interpolate-f11.svg'; md5=b1955b330bb469376a0f53627d6fb12e"
msgstr "@@image: 'interpolate-f11.svg'; md5=b1955b330bb469376a0f53627d6fb12e"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:149(None)
msgid "@@image: 'interpolate-f12.svg'; md5=f54c78050ee73b5ac8438dd9dfb26a58"
msgstr "@@image: 'interpolate-f12.svg'; md5=f54c78050ee73b5ac8438dd9dfb26a58"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:158(None)
msgid "@@image: 'interpolate-f13.svg'; md5=0e4c6243f3cecfe5c0c3d640c1df8433"
msgstr "@@image: 'interpolate-f13.svg'; md5=0e4c6243f3cecfe5c0c3d640c1df8433"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:167(None)
msgid "@@image: 'interpolate-f14.svg'; md5=85bacb3a295c8a8b74207f2433967ef6"
msgstr "@@image: 'interpolate-f14.svg'; md5=85bacb3a295c8a8b74207f2433967ef6"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:178(None)
msgid "@@image: 'interpolate-f15.svg'; md5=f24c95652fb988c9c2d71e6028ee1ffe"
msgstr "@@image: 'interpolate-f15.svg'; md5=f24c95652fb988c9c2d71e6028ee1ffe"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:197(None)
msgid "@@image: 'interpolate-f16.svg'; md5=b209d212b7e65ba40f49182ac54eab66"
msgstr "@@image: 'interpolate-f16.svg'; md5=b209d212b7e65ba40f49182ac54eab66"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:207(None)
msgid "@@image: 'interpolate-f17.svg'; md5=ed2c2700c8175dd6529e6cc29d43e7f8"
msgstr "@@image: 'interpolate-f17.svg'; md5=ed2c2700c8175dd6529e6cc29d43e7f8"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:216(None)
msgid "@@image: 'interpolate-f18.svg'; md5=0d4ffd793a48f69837394264b130f012"
msgstr "@@image: 'interpolate-f18.svg'; md5=0d4ffd793a48f69837394264b130f012"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:228(None)
msgid "@@image: 'interpolate-f19.svg'; md5=0645e0f056b2922d4f33909d9cdb12ac"
msgstr "@@image: 'interpolate-f19.svg'; md5=0645e0f056b2922d4f33909d9cdb12ac"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:237(None)
msgid "@@image: 'interpolate-f20.svg'; md5=0011eff8a109c5caf114ac98740ccd2d"
msgstr "@@image: 'interpolate-f20.svg'; md5=0011eff8a109c5caf114ac98740ccd2d"

#: tutorial-interpolate.xml:4(title)
msgid "Interpolate"
msgstr "Interpolation"

#: tutorial-interpolate.xml:5(author)
msgid "Ryan Lerch, ryanlerch at gmail dot com"
msgstr "Ryan Lerch, ryanlerch at gmail dot com"

#: tutorial-interpolate.xml:9(para)
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr ""
"Ce document explique comment utiliser l'extension d'interpolation d'Inkscape."

#: tutorial-interpolate.xml:14(title)
msgid "Introduction"
msgstr "Introduction"

#: tutorial-interpolate.xml:15(para)
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two or "
"more selected paths. It basically means that it “fills in the gaps” between "
"the paths and transforms them according to the number of steps given."
msgstr ""
"L'effet de cette extension consiste en une <firstterm>interpolation linéaire</"
"firstterm> entre deux (ou plus) chemins sélectionnés. Cela signifie "
"essentiellement qu'elle « comble les vides » entre les chemins et les "
"transforme en fonction du nombre d'étapes demandées."

#: tutorial-interpolate.xml:16(para)
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <command>Extensions &gt; Generate From Path &gt; "
"Interpolate</command> from the menu."
msgstr ""
"Pour utiliser l'extension Interpoler, sélectionnez les chemins que vous "
"souhaitez transformer, et choisissez la commande <command>Extensions &gt; "
"Générer à partir du chemin &gt; Interpoler</command>."

#: tutorial-interpolate.xml:17(para)
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <command>Path &gt; Object to Path</command> or <keycap>Shift+Ctrl"
"+C</keycap>. If your objects are not paths, the extension will do nothing."
msgstr ""
"Avant d'exécuter cette extension, les objets que vous souhaitez transformer "
"doivent être des <emphasis>chemins</emphasis>. Pour transformer un objet en "
"chemin, utilisez la commande <command>Chemin &gt; Objet en chemin</command> "
"ou <keycap>Maj+Ctrl+C</keycap>. Si vos objets ne sont pas des chemins, "
"l'extension n'aura pas d'effet."

#: tutorial-interpolate.xml:21(title)
msgid "Interpolation between two identical paths"
msgstr "Interpolation entre deux chemins identiques"

#: tutorial-interpolate.xml:22(para)
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""
"L'utilisation la plus simple de l'extension Interpoler consiste à l'appliquer "
"entre deux chemins identiques. Lorsque l'extension est appelée, elle a pour "
"effet de remplir l'espace entre les deux chemins avec des copies des chemins "
"originaux. Le nombre d'étapes définit combien de copies seront utilisées."

#: tutorial-interpolate.xml:23(para) tutorial-interpolate.xml:47(para)
msgid "For example, take the following two paths:"
msgstr "Prenons par exemple les deux chemins suivants :"

#: tutorial-interpolate.xml:32(para)
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr ""
"Sélectionnez maintenant les deux chemins et exécutez l'extension Interpoler "
"avec les paramètres proposés dans l'image suivante."

#: tutorial-interpolate.xml:41(para)
msgid ""
"As can be seen from the above result, the space between the two circle-shaped "
"paths has been filled with 6 (the number of interpolation steps) other circle-"
"shaped paths. Also note that the extension groups these shapes together."
msgstr ""
"Comme vous pouvez le voir ci-dessus, l'espace entre les deux chemins en forme "
"de cercle a été rempli par six (le nombre d'étapes d'interpolation) autres "
"chemins de même forme. Notez également que l'extension groupe toutes les "
"formes."

#: tutorial-interpolate.xml:45(title)
msgid "Interpolation between two different paths"
msgstr "Interpolation entre deux chemins différents"

#: tutorial-interpolate.xml:46(para)
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by the "
"Interpolation Steps value."
msgstr ""
"Lorsque l'interpolation est effectuée entre deux chemins différents, le "
"programme interpole la forme du chemin du premier vers le second. En "
"résultat, vous obtenez une séquence fondue entre les chemins, toujours avec "
"la régularité définie par la valeur du paramètre Étapes d'interpolation."

#: tutorial-interpolate.xml:56(para)
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr ""
"Sélectionnez maintenant les deux chemins et lancez l'extension Interpoler. Le "
"résultat devrait ressembler à ceci :"

#: tutorial-interpolate.xml:65(para)
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""
"Comme vous pouvez le constater avec le résultat précédent, l'espace entre le "
"chemin en forme de cercle et celui en forme de triangle a été rempli par six "
"chemins qui passent progressivement de la forme du premier chemin à celle du "
"second."

#: tutorial-interpolate.xml:67(para)
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is important. "
"To find the starting node of a path, select the path, then choose the Node "
"Tool so that the nodes appear and press <keycap>TAB</keycap>. The first node "
"that is selected is the starting node of that path."
msgstr ""
"Lorsque vous utilisez l'extension Interpoler entre deux chemins différents, "
"la <emphasis>position</emphasis> du nœud de départ de chaque chemin est "
"importante. Pour trouver ce nœud particulier, sélectionnez le chemin, puis "
"l'outil Nœuds pour faire apparaître les nœuds, et appuyez sur la touche "
"<keycap>Tab</keycap>. Le premier nœud apparaissant sélectionné est le nœud de "
"départ."

#: tutorial-interpolate.xml:69(para)
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""
"Examinez l'image ci-dessous, identique à l'exemple précédent, à l'exception "
"des nœuds ici affichés. Le nœud vert, sur chaque chemin, est le nœud de "
"départ."

#: tutorial-interpolate.xml:78(para)
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr ""
"L'exemple précédent (affiché à nouveau ci-dessous) a été réalisé avec ces "
"nœuds de départ."

#: tutorial-interpolate.xml:87(para)
msgid ""
"Now, notice the changes in the interpolation result when the triangle path is "
"mirrored so the starting node is in a different position:"
msgstr ""
"Notez maintenant les changements, dans le résultat de l'interpolation, "
"lorsque le chemin triangle a été inversé (par miroir) de façon à ce que le "
"nœud de départ soit dans une position différente :"

#: tutorial-interpolate.xml:106(title)
msgid "Interpolation Method"
msgstr "Méthode d'interpolation"

#: tutorial-interpolate.xml:107(para)
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in the "
"way that they calculate the curves of the new shapes. The choices are either "
"Interpolation Method 1 or 2."
msgstr ""
"Un des paramètres de l'extension Interpoler est la méthode d'interpolation. "
"Deux méthodes ont été implémentées ; elles se différencient dans la façon "
"dont sont calculées les courbes des nouvelles formes. Ces choix prennent la "
"valeur 1 ou 2."

#: tutorial-interpolate.xml:109(para)
msgid ""
"In the examples above, we used Interpolation Method 2, and the result was:"
msgstr ""
"Dans l'exemple ci-dessus, nous avons utilisé la méthode d'interpolation 2, et "
"le résultat était :"

#: tutorial-interpolate.xml:118(para)
msgid "Now compare this to Interpolation Method 1:"
msgstr "Comparez maintenant avec la méthode d'interpolation 1 :"

#: tutorial-interpolate.xml:127(para)
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""
"Les différences dans la manière dont ces méthodes calculent les nombres "
"dépassent le cadre de ce document. Essayez donc simplement les deux méthodes, "
"et utilisez celle qui donne le résultat le plus proche de ce que vous "
"souhaitez."

#: tutorial-interpolate.xml:132(title)
msgid "Exponent"
msgstr "Exposant"

#: tutorial-interpolate.xml:134(para)
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 0 makes the spacing between the "
"copies all even."
msgstr ""
"Le paramètre <firstterm>Exposant</firstterm> contrôle l'espacement entre les "
"étapes successives de l'interpolation. Avec un exposant à 0, l'espacement "
"entre les copies est partout identique."

#: tutorial-interpolate.xml:136(para)
msgid "Here is the result of another basic example with an exponent of 0."
msgstr "Voici le résultat d'un autre exemple élémentaire avec un exposant de 0."

#: tutorial-interpolate.xml:145(para)
msgid "The same example with an exponent of 1:"
msgstr "Même exemple avec un exposant de 1 :"

#: tutorial-interpolate.xml:154(para)
msgid "with an exponent of 2:"
msgstr "avec un exposant de 2 :"

#: tutorial-interpolate.xml:163(para)
msgid "and with an exponent of -1:"
msgstr "et avec un exposant de -1 :"

#: tutorial-interpolate.xml:172(para)
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""
"Lorsque vous utilisez l'exposant, l'<emphasis>ordre</emphasis> de sélection "
"des objets est important. Dans l'exemple précédent, le chemin en forme "
"d'étoile sur la gauche a été sélectionné en premier, et le chemin en forme "
"d'hexagone sur la droite sélectionné en second."

#: tutorial-interpolate.xml:174(para)
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 1:"
msgstr ""
"Voici le résultat d'une interpolation avec le chemin de droite sélectionné en "
"premier. L'exposant, dans cet exemple, a été positionné à 1 :"

#: tutorial-interpolate.xml:185(title)
msgid "Duplicate Endpaths"
msgstr "Dupliquer les extrémités"

#: tutorial-interpolate.xml:186(para)
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""
"Ce paramètre détermine si le groupe de chemins généré par l'extension "
"<emphasis>inclut une copie</emphasis> des chemins originaux sur lesquels "
"l'interpolation est appliquée."

#: tutorial-interpolate.xml:190(title)
msgid "Interpolate Style"
msgstr "Interpoler le style"

#: tutorial-interpolate.xml:191(para)
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each step. "
"So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""
"Ce paramètre est un des plus astucieux de l'extension. Il fait en sorte que "
"l'extension essaie de changer le style des chemins à chaque étape. Ainsi, si "
"le chemin original et le chemin final ont une couleur différente, les chemins "
"générés changeront de couleur progressivement à chaque étape."

#: tutorial-interpolate.xml:193(para)
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr ""
"Voici un exemple où la fonction Interpoler le style est utilisée sur le fond "
"d'un chemin :"

#: tutorial-interpolate.xml:203(para)
msgid "Interpolate Style also affects the stroke of a path:"
msgstr "Interpoler le style affecte également le contour d'un chemin :"

#: tutorial-interpolate.xml:212(para)
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr ""
"Bien sûr, les chemins de départ et de fin n'ont pas besoin d'être identiques :"

#: tutorial-interpolate.xml:223(title)
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr "Utiliser l'interpolation pour imiter des dégradés de forme irrégulière"

#: tutorial-interpolate.xml:225(para)
#, fuzzy
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was not "
"possible to create a gradient other than linear (straight line) or radial "
"(round). However, it could be faked using the Interpolate extension and "
"Interpolate Style. A simple example follows — draw two lines of different "
"strokes:"
msgstr ""
"Il n'est pas (encore) possible avec Inkscape de créer un dégradé d'une forme "
"autre que linéaire (en ligne droite) ou radiale (circulaire). Toutefois, "
"l'interpolation du style, dans cette extension, permet d'imiter un dégradé "
"irrégulier. Voici un exemple simple — dessinez deux lignes de contours "
"différents :"

#: tutorial-interpolate.xml:233(para)
msgid "And interpolate between the two lines to create your gradient:"
msgstr ""
"Et lancez une interpolation entre les deux lignes pour créer votre dégradé :"

#: tutorial-interpolate.xml:245(title)
msgid "Conclusion"
msgstr "Conclusion"

#: tutorial-interpolate.xml:246(para)
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful tool. "
"This tutorial covers the basics of this extension, but experimentation is the "
"key to exploring interpolation further."
msgstr ""
"Comme démontré ci-dessus, l'extension Interpoler est un outil puissant. Ce "
"didacticiel en couvre les bases, mais l'expérimentation est la clé pour "
"explorer l'interpolation plus profondément."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-interpolate.xml:0(None)
msgid "translator-credits"
msgstr ""
"jazzynico <nicoduf@yahoo.fr>, 2009-2014. Frigory <chironsylvain@orange.fr>, "
"2016."
