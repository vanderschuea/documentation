# translation of Inkscape tutorial interpolate to Vietnamese
# Copyright (C)
# This file is distributed under the same license as the Inkscape package.
# Nguyen Dinh Trung <nguyendinhtrung141@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tips and Tricks\n"
"POT-Creation-Date: 2018-02-15 21:50+0100\n"
"PO-Revision-Date: 2008-02-20 02:50+0100\n"
"Last-Translator: Nguyen Dinh Trung <nguyendinhtrung141@gmail.com>\n"
"Language-Team: Vietnamese <vi-bugs@googlegroups.com>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: tips-f06.svg:132(format) tips-f05.svg:554(format) tips-f04.svg:66(format)
#: tips-f03.svg:45(format) tips-f02.svg:45(format) tips-f01.svg:45(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:38(None)
msgid "@@image: 'tips-f01.svg'; md5=4d0784964d00a02e2b3e7d1e0cf24ee7"
msgstr "@@image: 'tips-f01.svg'; md5=4d0784964d00a02e2b3e7d1e0cf24ee7"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:56(None)
msgid "@@image: 'tips-f02.svg'; md5=5a822e44a71cbb8e091cd0fd12475df7"
msgstr "@@image: 'tips-f02.svg'; md5=5a822e44a71cbb8e091cd0fd12475df7"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:72(None)
msgid "@@image: 'tips-f03.svg'; md5=53a8a340a802c8ecb0370ac4d51ef2a0"
msgstr "@@image: 'tips-f03.svg'; md5=53a8a340a802c8ecb0370ac4d51ef2a0"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:120(None)
msgid "@@image: 'tips-f04.svg'; md5=1bcc5ce520dc9dd4c5f831a91169609f"
msgstr "@@image: 'tips-f04.svg'; md5=1bcc5ce520dc9dd4c5f831a91169609f"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:132(None)
msgid "@@image: 'tips-f05.svg'; md5=6ea20717ed0ed1cdab3f1124f8b04dec"
msgstr "@@image: 'tips-f05.svg'; md5=6ea20717ed0ed1cdab3f1124f8b04dec"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:149(None)
msgid "@@image: 'tips-f06.svg'; md5=a3bf8641bd7d6b8fd3473bb5c62da666"
msgstr "@@image: 'tips-f06.svg'; md5=a3bf8641bd7d6b8fd3473bb5c62da666"

#: tutorial-tips.xml:4(title)
msgid "Tips and Tricks"
msgstr ""

#: tutorial-tips.xml:7(para)
msgid ""
"This tutorial will demonstrate various tips and tricks that users have "
"learned through the use of Inkscape and some “hidden” features that can help "
"you speed up production tasks."
msgstr ""
"Bài hướng dẫn này sẽ giới thiệu cho bạn nhiều mẹo mực mà người dùng đã thực "
"hiện cùng Inkscape, cũng như một số tính năng \"ẩn\" có thể giúp bạn tăng tốc "
"độ làm việc."

#: tutorial-tips.xml:15(title)
#, fuzzy
msgid "Radial placement with Tiled Clones"
msgstr "Bố trí tròn các Bản sao Lát đều"

#: tutorial-tips.xml:16(para)
#, fuzzy
msgid ""
"It's easy to see how to use the <command>Create Tiled Clones</command> dialog "
"for rectangular grids and patterns. But what if you need <firstterm>radial</"
"firstterm> placement, where objects share a common center of rotation? It's "
"possible too!"
msgstr ""
"Ta có thể tạo các mẫu chữ nhật khá dễ dàng bằng hộp thoại <command>Tạo bản "
"sao đã lát đều</command>. Tuyệt vời hơn nữa, ta còn có thể tạo các "
"<firstterm>bố trí tròn</firstterm> có các đối tượng cùng quy chiếu đến 1 tâm "
"xoay."

#: tutorial-tips.xml:21(para)
#, fuzzy
msgid ""
"If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then "
"you can try the P3, P31M, P3M1, P4, P4M, P6, or P6M symmetries. These will "
"work nicely for snowflakes and the like. A more general method, however, is "
"as follows."
msgstr ""
"Nếu mẫu tròn của bạn chỉ có 3, 4, 6, 8, hoặc 12 thành phần, bạn có thể dùng "
"các phép đối xứng P3, P31M, P3M1, P4, P4M, P6, P6M. Ta sẽ thu được các hình "
"đối xứng tương tự bông tuyết. Ngoài ra, bạn có thể dùng phương pháp tổng quát "
"sau đây."

#: tutorial-tips.xml:26(para)
msgid ""
"Choose the P1 symmetry (simple translation) and then <emphasis>compensate</"
"emphasis> for that translation by going to the <command>Shift</command> tab "
"and setting <command>Per row/Shift Y</command> and <command>Per column/Shift "
"X</command> both to -100%. Now all clones will be stacked exactly on top of "
"the original. All that remains to do is to go to the <command>Rotation</"
"command> tab and set some rotation angle per column, then create the pattern "
"with one row and multiple columns. For example, here's a pattern made out of "
"a horizontal line, with 30 columns, each column rotated 6 degrees:"
msgstr ""
"Chọn phép đối xứng P1 (di chuyển đơn giản) và <emphasis>bổ sung</emphasis> "
"vào phép di chuyển này bằng cách chuyển sang thẻ <command>Dời chỗ</command> "
"và đặt <command>Mỗi hàng/Dời chỗ Y</command> và <command>Mỗi cột/Dời chỗ X</"
"command> thành -100%. Giờ tất cả các bản sao sẽ được sắp chồng lên nhau tại "
"vị trí gốc. Cuối cùng, ta chuyển sang thẻ <command>Xoay</command> và đặt một "
"góc xoay cho các cột, rồi tạo mẫu có 1 hàng và nhiều cột. Ví dụ, đây là một "
"mẫu được tạo ra bằng 1 đường thẳng nằm ngang, có 30 cột, mỗi cột xoay góc 6 "
"độ:"

#: tutorial-tips.xml:43(para)
msgid ""
"To get a clock dial out of this, all you need to do is cut out or simply "
"overlay the central part by a white circle (to do boolean operations on "
"clones, unlink them first)."
msgstr ""
"Để tạo ra 1 cái đồng hồ từ hình này, bạn chỉ việc cắt ra hoặc phủ lên phần "
"giữa một hình tròn trắng (bỏ liên kết giữa các bản sao trước khi để thực hiện "
"các phép toán tập hợp lên chúng)"

#: tutorial-tips.xml:47(para)
msgid ""
"More interesting effects can be created by using both rows and columns. "
"Here's a pattern with 10 columns and 8 rows, with rotation of 2 degrees per "
"row and 18 degrees per column. Each group of lines here is a “column”, so the "
"groups are 18 degrees from each other; within each column, individual lines "
"are 2 degrees apart:"
msgstr ""
"Khi dùng cả hàng và cột, bạn có thể tạo ra nhiều hiệu ứng thú vị hơn. Có một "
"mẫu 10 cột và 8 hàng, góc xoay là 2 độ đối với hàng và 18 độ đối với cột. Mỗi "
"nhóm đường ở đây là 1 “cột”, do vậy các nhóm xoay 18 độ so với nhau; đối với "
"các cột, các đường riêng biệt khác nhau 2 độ:"

#: tutorial-tips.xml:61(para)
#, fuzzy
msgid ""
"In the above examples, the line was rotated around its center. But what if "
"you want the center to be outside of your shape? Just click on the object "
"twice with the Selector tool to enter rotation mode. Now move the object's "
"rotation center (represented by a small cross-shaped handle) to the point you "
"would like to be the center of the rotation for the Tiled Clones operation. "
"Then use <command>Create Tiled Clones</command> on the object. This is how "
"you can do nice “explosions” or “starbursts” by randomizing scale, rotation, "
"and possibly opacity:"
msgstr ""
"Trong ví dụ trên, đường thẳng được xoay quanh tâm của nó. Nhưng nếu bạn muốn "
"tâm xoay đặt bên ngoài đối tượng thì sao? Chỉ việc tạo một hình chữ nhật vô "
"hình (không có màu tô và nét viền) bao quanh đối tượng, sao cho tâm hình chữ "
"nhật nằm tại tâm phép xoay, rồi nhóm đối tượng và hình chữ nhật lại, sau đó "
"dùng hiệu ứng <command>Tạo bản sao đã lát đều</command> cho nhóm này. Bạn sẽ "
"thu được các hình giống như “vụ nổ” hoặc “sao” khi điều chỉnh co giãn kích "
"thước, phép xoay và cả độ mờ đục:"

#: tutorial-tips.xml:79(title)
#, fuzzy
msgid "How to do slicing (multiple rectangular export areas)?"
msgstr "Làm thế nào để chia ảnh (dựa trên nhiều vùng hình chữ nhật)?"

#: tutorial-tips.xml:80(para)
#, fuzzy
msgid ""
"Create a new layer, in that layer create invisible rectangles covering parts "
"of your image. Make sure your document uses the px unit (default), turn on "
"grid and snap the rects to the grid so that each one spans a whole number of "
"px units. Assign meaningful ids to the rects, and export each one to its own "
"file (<command>File &gt; Export PNG Image</command> (<keycap>Shift+Ctrl+E</"
"keycap>)). Then the rects will remember their export filenames. After that, "
"it's very easy to re-export some of the rects: switch to the export layer, "
"use Tab to select the one you need (or use Find by id), and click Export in "
"the dialog. Or, you can write a shell script or batch file to export all of "
"your areas, with a command like:"
msgstr ""
"Tạo một lớp mới, trong đó có các hình chữ nhật vô hình bao lấy các phần ảnh "
"cần chia. Hãy đảm bảo là đơn vị đo bạn dùng là điểm ảnh (px - mặc định), bật "
"lưới và chế độ đính các hình chữ nhật vào trong lưới để mỗi hình tạo bởi 1 số "
"nguyên các ô lưới. Gán các tên hiệu (id) có ý nghĩa cho chúng để dễ phân "
"biệt, và xuất mỗi hình ra 1 tập tin riêng. Các hình chữ nhật sẽ lưu lại tên "
"tập tin được xuất ra tương ứng. Sau đó, ta có thể xuất lại một vài hình chữ "
"nhật: chuyển sang lớp xuất, dùng phím Tab để chọn hình chữ nhật bạn cần (hoặc "
"dùng lệnh Tìm dựa trên id) và nhấn Xuất trong hộp thoại. Hoặc, bạn cũng có "
"thể viết một văn lệnh hệ vỏ hoặc một tập tin lệnh gộp để xuất tất cả các vùng "
"đã đặt, với lệnh sau:"

#: tutorial-tips.xml:91(para)
#, fuzzy
msgid "inkscape -i area-id -t filename.svg"
msgstr "inkscape -i &lt;id-của-vùng&gt; -t &lt;tên-tập-tin.svg&gt;"

#: tutorial-tips.xml:94(para)
#, fuzzy
msgid ""
"for each exported area. The -t switch tells it to use the remembered filename "
"hint, otherwise you can provide the export filename with the -e switch. "
"Alternatively, you can use the <command>Extensions &gt; Web &gt; Slicer</"
"command> extensions, or <command>Extensions &gt; Export &gt; Guillotine</"
"command> for similar results."
msgstr ""
"cho mỗi vùng được xuất ra. Khoá -t báo với Inkscape dùng thông tin của vùng "
"làm tên tập tin xuất ra, hoặc bạn có thể cung cấp tên tập tin với khoá -e. "
"Một cách nữa, bạn có thể dùng tiện ích <ulink url=\"http://www."
"digitalunleashed.com/\">svgslice</ulink> để tự động xuất tài liệu SVG của "
"Inkscape, dùng lớp chia hoặc các đường gióng."

#: tutorial-tips.xml:102(title)
msgid "Non-linear gradients"
msgstr "Chuyển sắc phi tuyến"

#: tutorial-tips.xml:103(para)
msgid ""
"The version 1.1 of SVG does not support non-linear gradients (i.e. those "
"which have a non-linear translations between colors). You can, however, "
"emulate them by <firstterm>multistop</firstterm> gradients."
msgstr ""
"Phiên bản 1.1 của SVG không hỗ trợ các chuyển sắc phi tuyến (tức là chuyển "
"sắc có chuyển đổi màu không đồng nhất). Tuy nhiên, bạn có thể giả lập các "
"chuyển sắc loại này bằng chuyển sắc <firstterm>nhiều pha</firstterm>."

#: tutorial-tips.xml:108(para)
#, fuzzy
msgid ""
"Start with a simple two-stop gradient (you can assign that in the Fill and "
"Stroke dialog or use the gradient tool). Now, with the gradient tool, add a "
"new gradient stop in the middle; either by double-clicking on the gradient "
"line, or by selecting the square-shaped gradient stop and clicking on the "
"button <command>Insert new stop</command> in the gradient tool's tool bar at "
"the top. Drag the new stop a bit. Then add more stops before and after the "
"middle stop and drag them too, so that the gradient looks smooth. The more "
"stops you add, the smoother you can make the resulting gradient. Here's the "
"initial black-white gradient with two stops:"
msgstr ""
"Trước hết hãy dùng một chuyển sắc có 2 pha màu. Mở bộ sửa Chuyển sắc (bằng "
"cách bấm đúp chuột lên một chốt chuyển sắc bất kỳ với công cụ Chuyển sắc). "
"Thêm một pha chuyển sắc ở giữa, kéo nó lệch đi 1 chút. Sau đó thêm nhiều pha "
"màu khác trước và sau pha màu ở giữa và kéo chúng lệch đi, sao cho màu sắc "
"được chuyển đổi mượt mà. Càng nhiều pha được thêm vào, chuyển sắc bạn tạo ra "
"càng mượt hơn. Dưới đây là chuyển sắc 2 pha đen-trắng ban đầu:"

#: tutorial-tips.xml:125(para)
msgid ""
"And here are various “non-linear” multi-stop gradients (examine them in the "
"Gradient Editor):"
msgstr ""
"Và đây là rất nhiều chuyển sắc nhiều pha “phi tuyến” (hãy tìm hiểu chúng "
"trong Bộ sửa Chuyển sắc):"

#: tutorial-tips.xml:139(title)
msgid "Excentric radial gradients"
msgstr "Chuyển sắc tròn lệch tâm"

#: tutorial-tips.xml:140(para)
msgid ""
"Radial gradients don't have to be symmetric. In Gradient tool, drag the "
"central handle of an elliptic gradient with <keycap>Shift</keycap>. This will "
"move the x-shaped <firstterm>focus handle</firstterm> of the gradient away "
"from its center. When you don't need it, you can snap the focus back by "
"dragging it close to the center."
msgstr ""
"Các chuyển sắc tròn không nhất thiết phải đối xứng. Chọn công cụ Chuyển sắc, "
"kéo chốt nằm giữa của một chuyển sắc elip khi giữ <keycap>Shift</keycap>. "
"Thao tác này sẽ di chuyển <firstterm>chốt tiêu điểm</firstterm> hình chữ x "
"của chuyển sắc ra khỏi vị trí chính giữa. Khi bạn không cần hiệu ứng này nữa, "
"bạn có thể đính tiêu điểm trở lại bằng cách kéo nó gần về chính giữa."

#: tutorial-tips.xml:156(title)
msgid "Aligning to the center of the page"
msgstr "Sắp hàng vào giữa trang"

#: tutorial-tips.xml:157(para)
#, fuzzy
msgid ""
"To align something to the center or side of a page, select the object or "
"group and then choose <command>Page</command> from the <command>Relative to:</"
"command> list in the Align and Distribute dialog (<keycap>Shift+Ctrl+A</"
"keycap>)."
msgstr ""
"Để sắp hàng các đối tượng vào giữa trang, hãy chọn các đối tượng hoặc nhóm và "
"chọn <command>Trang</command> trong hộp <command>Tương đối với:</command> ở "
"hộp thoại Sắp hàng (<keycap>Ctrl+Shift+A</keycap>)."

#: tutorial-tips.xml:165(title)
msgid "Cleaning up the document"
msgstr "Làm sạch tài liệu"

#: tutorial-tips.xml:166(para)
#, fuzzy
msgid ""
"Many of the no-longer-used gradients, patterns, and markers (more precisely, "
"those which you edited manually) remain in the corresponding palettes and can "
"be reused for new objects. However if you want to optimize your document, use "
"the <command>Clean up Document</command> command in File menu. It will remove "
"any gradients, patterns, or markers which are not used by anything in the "
"document, making the file smaller."
msgstr ""
"Có rất nhiều các chuyển sắc, mẫu và hình nút thừa (chính xác hơn là các đối "
"tượng do bạn tự tay sửa) còn nằm trong bảng tương ứng, và có thể được dùng "
"lại cho các đối tượng mới. Tuy nhiên nếu bạn muốn tối ưu tài liệu của mình, "
"hãy dùng lệnh <command>Làm sạch defs</command> trong trình đơn Tập tin. Lệnh "
"này sẽ xoá các chuyển sắc, mẫu hoặc hình nút không được dùng tới trong tài "
"liệu của bạn, để cho tập tin của bạn nhỏ hơn."

#: tutorial-tips.xml:175(title)
msgid "Hidden features and the XML editor"
msgstr "Các tính năng ẩn và Bộ sửa XML"

#: tutorial-tips.xml:176(para)
#, fuzzy
msgid ""
"The XML editor (<keycap>Shift+Ctrl+X</keycap>) allows you to change almost "
"all aspects of the document without using an external text editor. Also, "
"Inkscape usually supports more SVG features than are accessible from the GUI. "
"The XML editor is one way to get access to these features (if you know SVG)."
msgstr ""
"Bộ sửa XML cho phép bạn thay đổi hầu hết các thành phần trong tài liệu mà "
"không cần dùng một chương trình soạn thảo văn bản bên ngoài nào khác. Ngoài "
"ra, Inkscape còn hỗ trợ nhiều tính năng SVG mà bạn không thể thực hiện được "
"thông qua giao diện đồ hoạ. Ví dụ, hiện SVG hỗ trợ biểu diễn mặt nạ và đường "
"nét bị xén, mặc dù không có công cụ trên giao diện đồ hoạ nào cho phép tạo "
"hoặc sửa chúng. Bộ sửa XML là cách duy nhất để bạn thực hiện các thao tác này "
"(nếu bạn biết rõ về định dạng SVG)."

#: tutorial-tips.xml:185(title)
msgid "Changing the rulers' unit of measure"
msgstr "Thay đổi đơn vị đo trên thước kẻ"

#: tutorial-tips.xml:186(para)
#, fuzzy
msgid ""
"In the default template, the unit of measure used by the rulers is mm. This "
"is also the unit used in displaying coordinates at the lower-left corner and "
"preselected in all units menus. (You can always hover your mouse over a ruler "
"to see the tooltip with the units it uses.) To change this, open "
"<command>Document Properties</command> (<keycap>Shift+Ctrl+D</keycap>) and "
"change the <command>Display units</command> on the <command>Page</command> "
"tab."
msgstr ""
"Trong mẫu tài liệu mặc định, đơn vị đo được dùng là điểm ảnh px (“đơn vị "
"người dùng SVG”, trong Inkscape nó tương đương với 0.8pt hay 1/90 inch). Đây "
"cũng là đơn vị được dùng để biểu diễn hệ toạ độ nằm trong góc dưới bên trái "
"và trong tất cả các trình đơn đơn vị. (Bạn luôn có thể di chuột lên trên "
"thước kẻ để xem thông điệp trợ giúp hiện lên thông báo về đơn vị đang được "
"dùng.) Để thay đổi đơn vị đo, hãy mở <command>Tuỳ thích Tài liệu</command> "
"(<keycap>Ctrl+Shift+D</keycap>) và chỉnh lại ô <command>Đơn vị mặc định</"
"command> trong thẻ <command>Trang</command>."

#: tutorial-tips.xml:197(title)
msgid "Stamping"
msgstr "Đóng dấu"

#: tutorial-tips.xml:198(para)
msgid ""
"To quickly create many copies of an object, use <firstterm>stamping</"
"firstterm>. Just drag an object (or scale or rotate it), and while holding "
"the mouse button down, press <keycap>Space</keycap>. This leaves a “stamp” of "
"the current object shape. You can repeat it as many times as you wish."
msgstr ""
"Để tạo ra nhiều bản sao của 1 đối tượng trong tài liệu, hãy dùng phương pháp "
"<firstterm>đóng dấu</firstterm>. Chỉ việc di chuyển đối tượng (hoặc co giãn "
"hay xoay nó), và trong khi vẫn giữ chuột, bạn nhấn <keycap>Phím cách</"
"keycap>. Thao tác này giống như “đóng dấu” xuống tài liệu, với hình dạng của "
"đối tượng chính là con dấu. Bạn có thể lặp đi lặp lại thao tác này nhiều lần."

#: tutorial-tips.xml:207(title)
msgid "Pen tool tricks"
msgstr "Các mẹo sử dụng công cụ Bút"

#: tutorial-tips.xml:208(para)
msgid ""
"In the Pen (Bezier) tool, you have the following options to finish the "
"current line:"
msgstr ""
"Với công cụ Bút (Bezier), bạn có các tuỳ chỉnh sau để kết thúc đường nét hiện "
"tại:"

#: tutorial-tips.xml:212(para)
msgid "Press <keycap>Enter</keycap>"
msgstr "Nhấn <keycap>Enter</keycap>"

#: tutorial-tips.xml:215(para)
msgid "Double click with the left mouse button"
msgstr "Bấm đúp chuột trái"

#: tutorial-tips.xml:218(para)
#, fuzzy
msgid "Click with the right mouse button"
msgstr "Bấm đúp chuột trái"

#: tutorial-tips.xml:221(para)
msgid "Select another tool"
msgstr "Chọn 1 công cụ khác"

#: tutorial-tips.xml:226(para)
msgid ""
"Note that while the path is unfinished (i.e. is shown green, with the current "
"segment red) it does not yet exist as an object in the document. Therefore, "
"to cancel it, use either <keycap>Esc</keycap> (cancel the whole path) or "
"<keycap>Backspace</keycap> (remove the last segment of the unfinished path) "
"instead of <command>Undo</command>."
msgstr ""
"Lưu ý rằng khi đường nét chưa hoàn thiện (tức là nét có màu xanh lục, và đoạn "
"đang vẽ màu đỏ) nó chưa tồn tại dưới dạng đối tượng trong tài liệu của bạn. "
"Vì thế, để xoá nó, hãy dùng <keycap>Esc</keycap> (xoá toàn bộ đường nét) hoặc "
"<keycap>Backspace</keycap> (xoá đoạn cuối cùng đang vẽ trong đường nét chưa "
"hoàn chỉnh) thay vì lệnh <command>Huỷ bước</command>."

#: tutorial-tips.xml:233(para)
msgid ""
"To add a new subpath to an existing path, select that path and start drawing "
"with <keycap>Shift</keycap> from an arbitrary point. If, however, what you "
"want is to simply <emphasis>continue</emphasis> an existing path, Shift is "
"not necessary; just start drawing from one of the end anchors of the selected "
"path."
msgstr ""
"Để thêm 1 đường nét thành phần cho 1 đường nét có sẵn, chọn đường nét đó và "
"bắt đầu vẽ khi giữ <keycap>Shift</keycap> từ 1 điểm bất kỳ. Tuy nhiên, nếu "
"bạn muốn <emphasis>vẽ tiếp</emphasis> thêm vào nét đang có, bạn không cần "
"phải giữ Shift; chỉ việc bắt đầu vẽ từ 1 nút cuối bên trong đường nét đang "
"chọn."

#: tutorial-tips.xml:242(title)
msgid "Entering Unicode values"
msgstr "Nhập các giá trị Unicode"

#: tutorial-tips.xml:243(para)
#, fuzzy
msgid ""
"While in the Text tool, pressing <keycap>Ctrl+U</keycap> toggles between "
"Unicode and normal mode. In Unicode mode, each group of 4 hexadecimal digits "
"you type becomes a single Unicode character, thus allowing you to enter "
"arbitrary symbols (as long as you know their Unicode codepoints and the font "
"supports them). To finish the Unicode input, press <keycap>Enter</keycap>. "
"For example, <keycap>Ctrl+U 2 0 1 4 Enter</keycap> inserts an em-dash (—). To "
"quit the Unicode mode without inserting anything press <keycap>Esc</keycap>."
msgstr ""
"Khi dùng công cụ Văn bản, nhấn <keycap>Ctrl+U</keycap> sẽ bật tắt giữa chế độ "
"Unicode và chế độ thường. Trong chế độ Unicode, mỗi nhóm 4 số hexa mà bạn gõ "
"sẽ chuyển thành 1 ký tự Unicode, nhờ đó bạn có thể nhập được các ký tự đặc "
"biệt (miễn là bạn biết mã Unicode và phông chữ hỗ trợ cho kiểu mã này). Để "
"kết thúc việc nhập Unicode, nhấn <keycap>Enter</keycap>. Ví dụ, <keycap>Ctrl"
"+U 2 0 1 4 Enter</keycap> sẽ chèn vào một đường gạch ngang dài (—)."

#: tutorial-tips.xml:252(para)
msgid ""
"You can also use the <command>Text &gt; Glyphs</command> dialog to search for "
"and insert glyphs into your document."
msgstr ""

#: tutorial-tips.xml:259(title)
msgid "Using the grid for drawing icons"
msgstr "Dùng lưới để vẽ biểu tượng"

#: tutorial-tips.xml:260(para)
#, fuzzy
msgid ""
"Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use "
"the <command>Document Preferences</command>) and set the grid to 0.5 px "
"(48x48 gridlines). Now, if you align filled objects to <emphasis>even</"
"emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> "
"gridlines with the stroke width in px being an even number, and export it at "
"the default 96dpi (so that 1 px becomes 1 bitmap pixel), you get a crisp "
"bitmap image without unneeded antialiasing."
msgstr ""
"Giả sử bạn muốn tạo một biểu tượng có kích thước 24x24 px. Hãy tạo 1 vùng vẽ "
"24x24 px (dùng hộp thoại <command>Tuỳ thích Tài liệu</command>) và đặt lưới "
"là 0.5 px (48x48 đường lưới). Giờ, nếu bạn sắp hàng các đối tượng vào các "
"đường lưới <emphasis>chẵn</emphasis>, và tô nét viền đối tượng vào các đường "
"lưới <emphasis>lẽ</emphasis> khi chọn độ rộng nét viền là 1 số nguyên px, và "
"xuất nó ra ở chế độ mặc định 90dpi (để 1 px trở thành 1 pixel bitmap), bạn sẽ "
"thu được một ảnh giòn mà không cần khử răng cưa."

#: tutorial-tips.xml:271(title)
msgid "Object rotation"
msgstr "Xoay đối tượng"

#: tutorial-tips.xml:272(para)
#, fuzzy
msgid ""
"When in the Selector tool, <keycap>click</keycap> on an object to see the "
"scaling arrows, then <keycap>click again</keycap> on the object to see the "
"rotation and skew arrows. If the arrows at the corners are clicked and "
"dragged, the object will rotate around the center (shown as a cross mark). If "
"you hold down the <keycap>Shift</keycap> key while doing this, the rotation "
"will occur around the opposite corner. You can also drag the rotation center "
"to any place."
msgstr ""
"Khi dùng công cụ Chọn, <keycap>bấm chuột</keycap> lên một đối tượng để hiển "
"thị các mũi tên co giãn, rồi <keycap>bấm lần nữa</keycap> để hiển thị các mũi "
"tên xoay và dịch chuyển. Nếu các mũi tên ở góc được bấm và di chuyển, đối "
"tượng sẽ bị xoay quanh tâm (dấu chữ thập). Nếu bạn giữ <keycap>Shift</keycap> "
"khi thực hiện thao tác này, phép xoay sẽ lấy tâm là góc đối diện của đối "
"tượng. Bạn cũng có thể di chuyển tâm xoay ra vị trí bất kỳ."

#: tutorial-tips.xml:281(para)
msgid ""
"Or, you can rotate from keyboard by pressing <keycap>[</keycap> and "
"<keycap>]</keycap> (by 15 degrees) or <keycap>Ctrl+[</keycap> and <keycap>Ctrl"
"+]</keycap> (by 90 degrees). The same <keycap>[]</keycap> keys with "
"<keycap>Alt</keycap> perform slow pixel-size rotation."
msgstr ""
"Hoặc, bạn có thể xoay đối tượng bằng bàn phím thông qua phím <keycap>[</"
"keycap> và <keycap>]</keycap> (mỗi lần 15 độ) hoặc <keycap>Ctrl+[</keycap> và "
"<keycap>Ctrl+]</keycap> (mỗi lần 90 độ). Cặp phím <keycap>[]</keycap> kết hợp "
"với phím <keycap>Alt</keycap> cho phép ta xoay các góc nhỏ theo mức điểm ảnh."

#: tutorial-tips.xml:290(title)
msgid "Drop shadows"
msgstr "Đổ bóng"

#: tutorial-tips.xml:291(para)
msgid ""
"To quickly create drop shadows for objects, use the <command>Filters &gt; "
"Shadows and Glows &gt; Drop Shadow...</command> feature."
msgstr ""

#: tutorial-tips.xml:295(para)
#, fuzzy
msgid ""
"You can also easily create blurred drop shadows for objects manually with "
"blur in the Fill and Stroke dialog. Select an object, duplicate it by "
"<keycap>Ctrl+D</keycap>, press <keycap>PgDown</keycap> to put it beneath "
"original object, place it a little to the right and lower than original "
"object. Now open Fill And Stroke dialog and change Blur value to, say, 5.0. "
"That's it!"
msgstr ""
"Inkscape có bộ lọc Làm mờ Gaussian cho SVG, để bạn dễ dàng tạo hiệu ứng đổ "
"bóng cho các đối tượng trong tài liệu. Chọn một đối tượng, nhân đôi nó lên "
"bằng <keycap>Ctrl+D</keycap>, nhấn <keycap>PgDown</keycap> để đem nó xuống "
"dưới đối tượng gốc, đặt nó hơi lệch xuống dưới và sang phải so với vị trí ban "
"đầu. Giờ hãy mở hộp thoại Tô và Nét, rồi thay đổi giá trị Làm mờ thành 5.0 "
"chả hạn."

#: tutorial-tips.xml:305(title)
msgid "Placing text on a path"
msgstr "Đặt văn bản theo đường nét"

#: tutorial-tips.xml:306(para)
msgid ""
"To place text along a curve, select the text and the curve together and "
"choose <command>Put on Path</command> from the Text menu. The text will start "
"at the beginning of the path. In general it is best to create an explicit "
"path that you want the text to be fitted to, rather than fitting it to some "
"other drawing element — this will give you more control without screwing over "
"your drawing."
msgstr ""
"Để đặt văn bản dọc theo 1 đường nét, chọn văn bản và đường nét rồi chọn lệnh "
"<command>Để trên đường nét</command> từ trình đơn Văn bản. Văn bản sẽ bắt đầu "
"từ đầu đường nét. Nói chung, bạn nên tạo riêng một đường nét để đặt văn bản "
"lên trên đó, hơn là dùng các thành phần khác có sẵn trong tài liệu — việc này "
"cho phép bạn dễ điều khiển hơn, mà không làm ảnh hưởng tới các thành phần "
"khác trong bản vẽ."

#: tutorial-tips.xml:316(title)
msgid "Selecting the original"
msgstr "Chọn đối tượng gốc"

#: tutorial-tips.xml:317(para)
msgid ""
"When you have a text on path, a linked offset, or a clone, their source "
"object/path may be difficult to select because it may be directly underneath, "
"or made invisible and/or locked. The magic key <keycap>Shift+D</keycap> will "
"help you; select the text, linked offset, or clone, and press <keycap>Shift"
"+D</keycap> to move selection to the corresponding path, offset source, or "
"clone original."
msgstr ""
"Nếu bạn có văn bản nằm trên đường nét, một đối tượng dời hình liên kết, hoặc "
"một bản sao, có thể việc chọn các đối tượng/đường nét gốc sẽ trở nên khó khăn "
"vì có thể chúng nằm ngay phía dưới hoặc/và bị khoá. Tổ hợp phím <keycap>Shift"
"+D</keycap> sẽ giúp bạn thực hiện việc chọn chúng; chọn văn bản, đối tượng "
"dời hình liên kết, hoặc bản sao, và nhấn <keycap>Shift+D</keycap> để chuyển "
"vùng chọn sang đối tượng đường nét, hình gốc tương ứng."

#: tutorial-tips.xml:327(title)
msgid "Window off-screen recovery"
msgstr "Khôi phục cửa sổ"

#: tutorial-tips.xml:328(para)
#, fuzzy
msgid ""
"When moving documents between systems with different resolutions or number of "
"displays, you may find Inkscape has saved a window position that places the "
"window out of reach on your screen. Simply maximise the window (which will "
"bring it back into view, use the task bar), save and reload. You can avoid "
"this altogether by unchecking the global option to save window geometry "
"(<command>Inkscape Preferences</command>, <command>Interface &gt; Windows</"
"command> section)."
msgstr ""
"Khi di chuyển tài liệu giữa các máy tinhs có độ phân giải khác nhau hoặc màn "
"hình hiển thị khác nhau, bạn có thể thấy rằng Inkscape đã lưu lại vị trí cửa "
"sổ nằm bên ngoài vùng hiển thị của màn hình. Chỉ việc phóng to tài liệu (để "
"hiển thị cửa sổ Inkscape trên màn hình, dùng thanh tác vụ), lưu lại và nạp "
"lại. Bạn có thể tránh hiện tượng này bằng cách bỏ tuỳ chọn toàn cục là lưu "
"lại vị trí cửa sổ (<command>Tùy thích Inkscape</command>, thẻ <command>Cửa "
"sổ</command>)."

#: tutorial-tips.xml:339(title)
msgid "Transparency, gradients, and PostScript export"
msgstr "Xuất Độ trong suốt, Chuyển sắc và PostScript"

#: tutorial-tips.xml:340(para)
#, fuzzy
msgid ""
"PostScript or EPS formats do not support <emphasis>transparency</emphasis>, "
"so you should never use it if you are going to export to PS/EPS. In the case "
"of flat transparency which overlays flat color, it's easy to fix it: Select "
"one of the transparent objects; switch to the Dropper tool (<keycap>F7</"
"keycap> or <keycap>d</keycap>); make sure that the <command>Opacity: Pick</"
"command> button in the dropper tool's tool bar is deactivated; click on that "
"same object. That will pick the visible color and assign it back to the "
"object, but this time without transparency. Repeat for all transparent "
"objects. If your transparent object overlays several flat color areas, you "
"will need to break it correspondingly into pieces and apply this procedure to "
"each piece. Note that the dropper tool does not change the opacity value of "
"the object, but only the alpha value of its fill or stroke color, so make "
"sure that every object's opacity value is set to 100% before you start out."
msgstr ""
"Các định dạng PostScript và EPS không hỗ trợ <emphasis>độ trong suốt</"
"emphasis>, nên bạn cần tránh dùng giá trị trong suốt nếu bạn xuất ra dạng PS/"
"EPS. Trong trường hợp độ trong suốt phủ lên màu đồng nhất, ta có thể dễ dàng "
"khắc phục nhược điểm này: Chọn một trong các đối tượng trong suốt; chuyển "
"sang công cụ Bút chọn màu (<keycap>F7</keycap>); đảm bảo rằng chế độ lấy màu "
"là “lấy màu hiện không có alpha”"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tips.xml:0(None)
msgid "translator-credits"
msgstr "Nguyen Dinh Trung <nguyendinhtrung141@gmail.com>, 2007, 2008."

#, fuzzy
#~ msgid "Select the Pen tool from the toolbar"
#~ msgstr "Chọn công cụ Bút một lần nữa"

#~ msgid ""
#~ "Exporting <emphasis>gradients</emphasis> to PS or EPS does not work for "
#~ "text (unless text is converted to path) or for stroke paint. Also, since "
#~ "transparency is lost on PS or EPS export, you can't use e.g. a gradient "
#~ "from an <emphasis>opaque</emphasis> blue to <emphasis>transparent</"
#~ "emphasis> blue; as a workaround, replace it by a gradient from "
#~ "<emphasis>opaque</emphasis> blue to <emphasis>opaque</emphasis> background "
#~ "color."
#~ msgstr ""
#~ "Xuất <emphasis>chuyển sắc</emphasis> dưới dạng PS hay EPS không làm việc "
#~ "đối với văn bản (trừ khi văn bản đã được chuyển đổi sang đường nét) hoặc "
#~ "sơn nét. Ngoài ra, vì khi xuất PS hay EPS, độ trong suốt bị mất đi, bạn "
#~ "không thể dùng các loại chuyển sắc kiểu như biến đổi từ xanh "
#~ "<emphasis>trong suốt</emphasis> sang xanh <emphasis>đục</emphasis> được; "
#~ "để khắc phục, thay thế nó bằng một chuyển sắc từ xanh <emphasis>đục</"
#~ "emphasis> tới màu nền <emphasis>đục</emphasis> tương ứng."

#~ msgid "Clipping or masking a bitmap"
#~ msgstr "Xén hoặc phủ mặt nạ lên một ảnh bitmap"

#, fuzzy
#~ msgid ""
#~ "By default, an imported bitmap (e.g. a photo) is an image element which is "
#~ "not editable by the Node tool. To work around this, convert the image into "
#~ "a rectangle with pattern fill by <command>Object to Pattern</command> "
#~ "(<keycap>Alt+I</keycap>). This will give you a rectangle <emphasis>filled</"
#~ "emphasis> with your bitmap. Now this object can be converted to path, node-"
#~ "edited, intersected with other shapes etc. In <command>Inkscape "
#~ "Preferences</command> (<command>Misc</command> tab), you can set the "
#~ "option of always importing bitmaps as pattern-filled rectangles."
#~ msgstr ""
#~ "Theo mặc định, một ảnh bitmap được nhập vào là một thành phần &lt;ảnh&gt; "
#~ "mà ta không thể dùng công cụ Nút để chỉnh sửa được. Để xén hoặc phủ mặt nạ "
#~ "cho nó, bạn hãy chuyển nó thành 1 hình chữ nhật có mẫu tô bằng lệnh "
#~ "<command>Đối tượng thành mẫu</command> (<keycap>Alt+I</keycap>). Lệnh này "
#~ "tạo ra một hình chữ nhật được <emphasis>tô</emphasis> bằng ảnh bitmap đã "
#~ "nhập. Giờ đối tượng này có thể được chuyển sang đường nét, và sửa lại bằng "
#~ "công cụ Nút, lấy phần giao với các đối tượng khác, v..v... Trong hộp thoại "
#~ "<command>Tuỳ chỉnh Inkscape</command>(thẻ <command>Linh tinh</command>), "
#~ "bạn có thể bật tuỳ chọn luôn nhập ảnh bitmap thành hình chữ nhật được tô "
#~ "bằng mẫu."

#~ msgid "Open dialog as an object palette"
#~ msgstr "Hộp thoại Mở ở dạng bảng đối tượng"

#~ msgid ""
#~ "If you have a number of small SVG files whose contents you often reuse in "
#~ "other documents, you can conveniently use the Open dialog as a palette. "
#~ "Add the directory with your SVG sources into the bookmarks list so you can "
#~ "open it quickly. Then browse that directory looking at the previews. Once "
#~ "you found the file you need, simply drag it to the canvas and it will be "
#~ "imported into your current document."
#~ msgstr ""
#~ "Nếu bạn có nhiều các tập tin SVG nhỏ hay được dùng lại trong các tài liệu "
#~ "khác, bạn có thể dùng hộp thoại Mở như một bảng đối tượng. Thêm thư mục "
#~ "chứa các tập tin SVG nguồn vào trong danh sách đánh dấu để bạn có thể "
#~ "nhanh chóng mở thư mục đó ra. Sau đó duyệt thư mục và xem ảnh xem trước. "
#~ "Sau khi tìm ra tập tin cần mở, bạn chỉ việc kéo nó vào trong vùng vẽ và nó "
#~ "sẽ được nhập vào trong tài liệu hiện hành."

#~ msgid "TIPS AND TRICKS"
#~ msgstr "MẸO DÙNG"
