# Makefile written by 
# Colin Marquardt <colin@marquardt-home.de>, 2007.

# This Makefile stub is meant to be included from a tutorial subdirectory,
# therefore we need the ".." in some places.

# some tool definitions
XML2PO       := xml2po
INKSCAPEPATH := inkscape

# use a clean profile for Inkscape to minimize side-effects
export INKSCAPE_PROFILE_DIR=$(CURDIR)/../.inkscape_profile_dir

# the current path (which will be inside a tutorial directory) 
# with the slashes replaced by spaces
PATHELEMS := $(subst /, ,$(PWD))
# the last element of the path with spaces is the tutorial name
TUTORIAL := $(word $(words $(PATHELEMS)), $(PATHELEMS))

# the existing po files are used to determine which HTML and SVG files to build
SRC := $(wildcard *.po)

# all languages for which a po file has been found
ifdef MYLANG
MYLANGS := $(MYLANG)
else
MYLANGS := $(SRC:.po=)
endif

ifdef MYLANG
TARGETS_HTML := tutorial-$(TUTORIAL).$(MYLANG).html
TARGETS_SVG := tutorial-$(TUTORIAL).$(MYLANG).svg
else
# generate filenames for HTML and SVG tutorials
# Example: "tutorial-shapes. + de.html" --> tutorial-shapes.de.html
TARGETS_HTML := $(addprefix tutorial-$(TUTORIAL)., $(SRC:.po=.html))
# Example: "tutorial-shapes. + de.svg" --> tutorial-shapes.de.svg
TARGETS_SVG  += $(addprefix tutorial-$(TUTORIAL)., $(SRC:.po=.svg))
endif

.PHONY: all
all: html svg

.PHONY: html
html: $(TARGETS_HTML)

.PHONY: svg
svg: $(TARGETS_SVG)

# FIXME: make targets dependent on Makefile itself

tutorial-$(TUTORIAL).%.html: tutorial-$(TUTORIAL).%.xml ../tutorial-html.xsl ../make-html images-png images-%-png 
# Example: ./make-html basic /usr/bin/inkscape es
	cd .. && ./make-html $(TUTORIAL) $(INKSCAPEPATH) $*

tutorial-$(TUTORIAL).%.svg: tutorial-$(TUTORIAL).%.xml ../tutorial-svg.xsl ../make-svg images-%
	cd .. && ./make-svg $(TUTORIAL) $(INKSCAPEPATH) $*

# generate the localized XML version of the tutorial:
.PRECIOUS: tutorial-$(TUTORIAL).%.xml  # keep the translated XML files around (no need to delete)
tutorial-$(TUTORIAL).%.xml: %.po tutorial-$(TUTORIAL).xml
	$(XML2PO) --po-file=$< tutorial-$(TUTORIAL).xml > $@

# find translatable images
ALL_IMAGES := $(basename $(wildcard $(TUTORIAL)-f??.svg))
TRANSLATABLE_IMAGES := $(shell grep -il $(TUTORIAL)-f??.svg -e '<text\|<tspan\|<flowroot' | cut -f 1 -d '.')

# create localized versions of all translatable images
# Example: make images-de
.PHONY: images-%
images-%: $(addsuffix -%.svg, $(TRANSLATABLE_IMAGES)) ;

.PHONY: images-png
images-png: $(addsuffix .svg, $(ALL_IMAGES))
	@for file in $(ALL_IMAGES); do \
		([ -f $$file.png ] && [ $$file.png -nt $$file.svg ]) || remake=1; \
	done; \
	if [ -n "$$remake" ]; then \
		echo "Making $(addsuffix .png, $(ALL_IMAGES))"; \
		for file in $(ALL_IMAGES); do \
			echo "--file=$$file.svg --export-area-drawing --export-dpi=180 --export-png=$$file.png"; \
		done | $(INKSCAPEPATH) --shell > /dev/null; \
	fi
	
.PHONY: images-%-png
images-%-png: images-%
	@for file in $(TRANSLATABLE_IMAGES); do \
		([ -f $$file-$*.png ] && [ $$file-$*.png -nt $$file-$*.svg ]) || remake=1; \
	done; \
	if [ -n "$$remake" ]; then \
		echo "Making $(addsuffix -$*.png, $(TRANSLATABLE_IMAGES))"; \
		for file in $(TRANSLATABLE_IMAGES); do \
			echo "--file=$$file-$*.svg --export-area-drawing --export-dpi=180 --export-png=$$file-$*.png"; \
		done | $(INKSCAPEPATH) --shell > /dev/null; \
	fi

# Generate implicit rules (one target per untranslated file)
# to create localized versions for all translatable images.
#   will result in targets like
#     shapes-f01-%.svg: %.po
#   which can be called with
#     make shapes-f01-de.svg
define GEN_IMAGE_RULE
.PRECIOUS: $(1)-%.svg  # keep the translated images around (no need to delete)
$(1)-%.svg: $(1).svg %.po
	$$(XML2PO) --po-file=$$*.po $$< > $$@

	@# these are some manual hacks to get localized PNGs included in SVGs working
	@# TODO: check if this can be simplified / generalized
	@if [ '$(TUTORIAL)' = 'tracing' ] && [ -f potrace.$$*.png ] && grep -q 'potrace.png' $$@; then \
		sed -e "s/potrace.png/potrace.$$*.png/g" $$@ > $$@.temp.svg && mv $$@.temp.svg $$@; \
	fi
	@if [ '$(TUTORIAL)' = 'tracing-pixelart' ] && [ -f pixelart-dialog.$$*.png ] && grep -q 'pixelart-dialog.png' $$@; then \
		sed -e "s/pixelart-dialog.png/pixelart-dialog.$$*.png/g" $$@ > $$@.temp.svg && mv $$@.temp.svg $$@; \
	fi
endef
$(foreach IMAGE,$(TRANSLATABLE_IMAGES),$(eval $(call GEN_IMAGE_RULE,$(IMAGE))))


.PHONY: update-po
update-po: tutorial-$(TUTORIAL).pot check_mylang
	msgmerge $(MYLANG).po tutorial-$(TUTORIAL).pot > updated.$(MYLANG).po
	mv updated.$(MYLANG).po $(MYLANG).po

# FIXME: how to trigger an update for all languages?
# tutorial-%.xml has been updated and you want the new stuff in your %.po:
%.po: tutorial-$(TUTORIAL).xml $(TUTORIAL)-f??.svg
	$(XML2PO) --update-translation=$@ tutorial-$(TUTORIAL).xml $(TUTORIAL)-f??.svg
	msgcat --width=80 --output-file=$@ $@

# use this at the very beginning, when there is no translation at all
.PHONY: pot
pot: tutorial-$(TUTORIAL).pot

tutorial-$(TUTORIAL).pot: tutorial-$(TUTORIAL).xml $(TUTORIAL)-f??.svg
	$(XML2PO) --output=$@ $^

# use this when there is a *.%.xml, but you want a %.po:
#%.po: tutorial-$(TUTORIAL).%.xml
#	$(XML2PO) --output=$@ --reuse=$* tutorial-$(TUTORIAL).xml


# This target is not as effective as it could be since xml2po cleverly "repairs"
# certain wrong markup
.PHONY: check
check: tutorial-$(TUTORIAL).$(MYLANG).xml $(MYLANG).po check_mylang
	xmllint --valid --noout tutorial-$(TUTORIAL).$(MYLANG).xml

# Some SVG files don't use the usual po mechanism. This target scours them.
.PHONY: no-po
NOPOS := $(wildcard *.svg)
no-po:
	for NOPO in $(NOPOS); do \
		scour -i $${NOPO} -o scoured/$${NOPO} --keep-editor-data --disable-embed-rasters; \
	done; \
	mkdir -p ../../export-inkscape/tutorials/
	cp scoured/*.svg ../../export-inkscape/tutorials/

.PHONY: copy
copy: copy-svg copy-html

.PHONY: copy-html
copy-html:
#	@echo ""
#	@echo "In order to copy the HTML versions of the tutorials to the directory where Inkscape can find them, try:"
	mkdir -p ../../export-website/tutorials/$(TUTORIAL)
	cp *.html ../../export-website/tutorials/$(TUTORIAL)/
	mv ../../export-website/tutorials/$(TUTORIAL)/tutorial-$(TUTORIAL).en.html ../../export-website/tutorials/$(TUTORIAL)/tutorial-$(TUTORIAL).html
	cp *.png ../../export-website/tutorials/$(TUTORIAL)/
	cp ../*.css ../../export-website/tutorials/

.PHONY: copy-svg
copy-svg:
#	@echo ""
#	@echo "In order to copy the SVG versions of the tutorials to the directory where Inkscape can find them, try:"
	mkdir -p ../../export-inkscape/tutorials/
	cp tutorial-$(TUTORIAL).*.svg ../../export-inkscape/tutorials/
	mv ../../export-inkscape/tutorials/tutorial-$(TUTORIAL).en.svg ../../export-inkscape/tutorials/tutorial-$(TUTORIAL).svg
	@if [ -r tux.png ]; then \
		cp tux.png oldguitar.jpg potrace*.png ../../export-inkscape/tutorials/ ;\
	fi
	@if [ -r pixelart-dialog.png ]; then \
		cp pixelart-dialog*.png ../../export-inkscape/tutorials/ ;\
	fi

.PHONY: clean-export
clean-export: clean-html-export clean-svg-export

.PHONY: clean-html-export
clean-html-export:
	rm -f ../../export-website/tutorials/$(TUTORIAL)/*

.PHONY: clean-svg-export
ifneq "$(TUTORIAL)" "tracing"
clean-svg-export:
	rm -f ../../export-inkscape/tutorials/tutorial-$(TUTORIAL)*
else
clean-svg-export:
	rm -f ../../export-inkscape/tutorials/tutorial-$(TUTORIAL)*
	rm -f ../../export-inkscape/tutorials/*.png
	rm -f ../../export-inkscape/tutorials/*.jpg
endif

.PHONY: clean
clean: clean-html clean-svg
	rm -rf $(CURDIR)/../.inkscape_profile_dir

.PHONY: clean-html
clean-html: clean-xml
	rm -f *.html
	rm -f $(TUTORIAL)-f*.png

.PHONY: clean-svg
clean-svg: clean-xml
	rm -f tutorial-$(TUTORIAL)*.svg
	rm -f $(TUTORIAL)-*-*.svg

.PHONY: clean-xml
clean-xml:
	rm -f tutorial-$(TUTORIAL).*.xml

.PHONY: help
help:
	@echo "Targets:"
	@echo "   all [MYLANG=<LANG>]     - Create HTML and SVG files (can be restricted to <LANG>)"
	@echo "   html [MYLANG=<LANG>]    - Create HTML files (can be restricted to <LANG>)"
	@echo "   svg [MYLANG=<LANG>]     - Create SVG files (can be restricted to <LANG>). You will want to use the 'images' target before this."
	@echo "   no-po                   - Optimize handmade SVG files with scour."
	@echo "   pot                     - Create a tutorial-*.pot file for translators (who will copy it to <LANG>.po)"
	@echo "   update-po MYLANG=<LANG> - Update all po files for language <LANG>"
	@echo "   check MYLANG=<LANG>     - Run xmllint on the generated XML file for language <LANG>"
	@echo "   copy-svg                - Instructions for copying generated SVG tutorials to the correct directory"
	@echo "   copy-html               - Instructions for copying generated HTML tutorials to the correct directory"
	@echo "   copy                    - Instructions for copying both SVG and HTML tutorials"
	@echo "   clean                   - Clean all generated files"
	@echo "   clean-html-export       - Instructions for cleaning the files in the website export directory"
	@echo "   clean-svg-export        - Instructions for cleaning the files in the SVG export directory"
	@echo "   clean-svg               - Instructions for cleaning the SVG files in the tutorial directory"
	@echo "   clean-html              - Instructions for cleaning the HTML files in the tutorial directory"

.PHONY: check_mylang
check_mylang:
ifndef MYLANG
	@echo "E:Syntax error - you didn't define MYLANG."
	@echo ""
	@$(MAKE) -s help
	@exit 1
else
	@if [ ! -r $(MYLANG).po ]; then \
	echo "E:Language file '$(MYLANG).po' does not exist!"; \
	echo ""; \
	exit 1; \
	fi
endif

# TODO: check pofilter from translate-toolkit
# TODO: make update-po work on all languages
