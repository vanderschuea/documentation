#!/usr/bin/env bash

set -eu

# //// Functions ////

#
# usage() : short help
#
usage() {
    cat <<EOF
Usage :
    $0 [-h|--help] [-d|--docs-path PATH-TO-INKSCAPE-DOCS] [-i|--inkscape-path PATH-TO-INKSCAPE] [-w|--website-path PATH-TO-INKSCAPE-WEB] [-v|--version INKSCAPE-VERSION] [languages]
EOF
}

#
# count_msgids() : count the original strings
#
count_msgids() {
    cat | grep -E '^msgid\s+' | wc -l
}

#
# count_original_words() : count the words in the original strings
#
count_original_words() {
    cat | grep ^msgid | sed 's/^msgid "//g;s/"$//g' | wc -w
}

#
# statistics() : process statistics on translations
#
statistics() {
    if [ -e "${current_po_dir}/${tmplang}" ]; then
        PO_FILES="$(mktemp -t XXXXXXXXXX.pofiles)"
        echo "$current_po_dir/$tmplang" > $PO_FILES
        PO_MESSAGES="$(mktemp -t XXXXXXXXXX.po)"
        msgcat --files-from=$PO_FILES --output=$PO_MESSAGES
        REV_DATE=$(grep "PO-Revision-Date: [1-9]" $PO_MESSAGES | cut -d " " -f2)
        TOTAL=$(msgattrib --force-po --no-obsolete $PO_MESSAGES | count_msgids)
        TOTAL_WC=$(msgattrib --force-po --no-obsolete --no-wrap $PO_MESSAGES | count_original_words)
        FUZZY=$(msgattrib --force-po --only-fuzzy --no-obsolete $PO_MESSAGES | count_msgids)
        # Fully translated files always return one extra fuzzy entry.
        FUZZY=$((FUZZY-1))
        TRANSLATED=$(msgattrib --force-po --translated --no-fuzzy --no-obsolete $PO_MESSAGES | count_msgids)
        TRANSLATED_WC=$(msgattrib --force-po --translated --no-fuzzy --no-obsolete --no-wrap $PO_MESSAGES | count_original_words)
        rm -f $PO_FILES $PO_MESSAGES
        [[ $TRANSLATED -eq $TOTAL ]] && COMPLETE=1 || COMPLETE=
    else
        >&2 echo "Error: no $tmplang translation file found in ${current_po_dir}"
        TOTAL=0
    fi
}

#
# colors() : turn percentage into nice RGB color
#
#   parameters:
#     - $1 percentage (0-100)
#
#   to test:
#     for p in {0..100}; do colors $p; echo "<span style='color:rgb($color_red,$color_green,$color_blue)'>text 123 ($p%)</span><br/>"; done > test.html
colors() {
    if [[ $1 -lt 60 ]]; then
        color_red=255
        color_green=0
    elif [[ $1 -lt 80 ]]; then
        color_red=$((255-($1-60)*32/20))
        color_green=$((($1-60)*224/20))
    else
        color_red=$((224-($1-80)*224/20))
        color_green=$((224-($1-80)*96/20))
    fi
    color_blue=0
}

#
# make_heading() : create heading
#
#   parameters:
#     - $1 level
#     - $2 id
#     - $3 text
#
make_heading() {
    echo "
    <h$1 id='$2'>$3</h$1>"
}

#
# make_table() : create table with statistics for all .po files in array ${LANGUAGES}
#
make_table() {
    echo "    <table>
      <thead>
        <tr>
          <td>Language</td><td>Status</td><td>Untranslated</td><td>Fuzzy</td><td>Total</td><td>Last changed</td>
        </tr>
      </thead>
      <tbody>"
    printf "   " 1>&2
    for tmplang in ${LANGUAGES[@]} ; do
        LANG_CODE="${tmplang%%/*}"
        LANG_CODE="${LANG_CODE%.po}"
        LANG_CODE="${LANG_CODE#pod.}"
        printf "${LANG_CODE} " 1>&2
        statistics
        colors $(($TRANSLATED*100/$TOTAL))
        if [ "$TOTAL" != "0" -a "$LANG_CODE" != "en" ]; then
            echo "       " \
        "<tr${COMPLETE:+ class='complete'}>
          <td>$LANG_CODE</td>
          <td><progress max='100' value='$(($TRANSLATED*100/$TOTAL))' title='$(($TRANSLATED*100/$TOTAL))%'>$(($TRANSLATED*100/$TOTAL))%</progress></td>
          <td style='color:rgb($color_red,$color_green,$color_blue)'>$(($TOTAL-$TRANSLATED-$FUZZY)) ($((($TOTAL-$TRANSLATED-$FUZZY)*100/$TOTAL))%)</td>
          <td style='color:rgb($color_red,$color_green,$color_blue)'>$FUZZY ($(($FUZZY*100/$TOTAL))%)</td>
          <td>$TRANSLATED / $TOTAL</td>
          <td>$REV_DATE</td>
        </tr>"
        fi
    done
    printf "\n" 1>&2
    echo "      </tbody>
    </table>"
}

#
# stats_ui() : statistics for the UI translation files
#
stats_ui() {
    echo "Creating translation statistics for interface" 1>&2

    current_po_dir=$inkscape_dir/po
    if [ "$user_lang" ]
        then
            LANGUAGES=("${user_lang[@]/%/.po}")
        else
            LANGUAGES="$(ls -1 "$current_po_dir/" | grep "\.po$" | sort)"
    fi

    make_heading 2 "ui" "User Interface"
    make_table
}

#
# stats_man() : statistics for the man translation files
#
stats_man() {
    echo "Creating translation statistics for manpages" 1>&2

    current_po_dir=$docs_dir/man
    if [ "$user_lang" ]
        then
            LANGUAGES=("${user_lang[@]/#/pod.}")
            LANGUAGES=("${LANGUAGES[@]/%/.po}")
        else
            LANGUAGES="$(ls -1 "$current_po_dir/" | grep "\.po$" | sort)"
    fi

    make_heading 2 "man" "Man pages"
    make_table
}

#
# stats_keys() : statistics for the keys and mouse reference translation files
#
stats_keys() {
    echo "Creating translation statistics for keyboard and mouse reference" 1>&2

    current_po_dir=$docs_dir/keys
    if [ "$user_lang" ]
        then LANGUAGES=("${user_lang[@]/%/.po}")
        else LANGUAGES="$(ls -1 "$current_po_dir/" | grep "\.po$" | sort)"
    fi

    make_heading 2 "keys" "Keys and mouse reference"
    make_table
}

#
# stats_tutorials() : statistics for the tutorials
#
stats_tutorials() {
    echo "Creating translation statistics for tutorials" 1>&2

    make_heading 2 "tutorial" "Tutorials"
    for tuto in ${tuto_list[@]} ; do
        echo "-> tutorial $tuto" 1>&2

        current_po_dir=$docs_dir/tutorials/$tuto
        if [ "$user_lang" ]
            then LANGUAGES=("${user_lang[@]/%/.po}")
            else LANGUAGES="$(ls -1 "$current_po_dir/" | grep "\.po$" | sort)"
        fi

        make_heading 3 "tutorial-$tuto" "$tuto"
        make_table
    done
}

#
# stats_website() : statistics for the website translation files
#
stats_website() {
    echo "Creating translation statistics for website" 1>&2

    current_po_dir=$website_dir
    if [ "$user_lang" ]
        then
            LANGUAGES=("${user_lang[@]/%//LC_MESSAGES/django\.po}")
        else
            LANGUAGES=($(cd $current_po_dir && ls -1d */ | sort))
            LANGUAGES=(${LANGUAGES[@]/%/LC_MESSAGES/django\.po})
    fi

    make_heading 2 "website" "Website"
    make_table
}


# //// Main program ////

user_lang=
docs_dir=$(realpath "$(pwd)/..")
inkscape_dir=$(realpath "$(pwd)/inkscape.git")
website_dir=$(realpath "$(pwd)/web-i18n.git")
inkscape_version="master"
tuto_list=(advanced basic calligraphy elements interpolate shapes tips tracing tracing-pixelart)

# Command line options
while test $# -gt 0
do
    case $1 in
    -h | --help)
        usage
        exit 0
        ;;
    -d | --docs-dir)
        shift
        docs_dir=$(realpath $1)
        ;;
    -i | --inkscape-dir)
        shift
        inkscape_dir=$(realpath $1)
        ;;
    -w | --website-dir)
        shift
        website_dir=$(realpath $1)
        ;;
    -v | --version)
        shift
        inkscape_version=$1
        ;;
    -*)  echo "$0 : invalid option $1" 1>&2
        usage
        exit 1
        ;;
    *)
        user_lang=( "$@" )
        break
        ;;
    esac
    shift
done

echo "<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>Translation statistics for Inkscape $inkscape_version</title>
    <link rel='stylesheet' href='statistics.css' media='screen' type='text/css' />
  </head>
  <body>
    <div class='po-stats'>

    <p>Last updated: $(date -R -u | cut -f1 -d+)</p>

    <div id='toc'>
      <ul>
        <li><a href='#ui'>User interface</a></li>
        <li><a href='#man'>Man pages</a></li>
        <li><a href='#keys'>Keys and mouse references</a></li>
        <li>
          <a href='#tutorial'>Tutorials</a>
          <ul>
            <li><a href='#tutorial-advanced'>Advanced</a></li>
            <li><a href='#tutorial-basic'>Basic</a></li>
            <li><a href='#tutorial-calligraphy'>Calligraphy</a></li>
            <li><a href='#tutorial-elements'>Elements of design</a></li>
            <li><a href='#tutorial-interpolate'>Interpolate</a></li>
            <li><a href='#tutorial-shapes'>Shapes</a></li>
            <li><a href='#tutorial-tips'>Tips</a></li>
            <li><a href='#tutorial-tracing'>Tracing</a></li>
            <li><a href='#tutorial-tracing-pixelart'>Tracing pixel art</a></li>
          </ul>
        </li>
        <li><a href='#website'>Website</a></li>
      </ul>
    </div>"

# Processing stat tables
[ -d "$inkscape_dir/po" ]    && stats_ui        || echo "WARNING: $inkscape_dir/po not found. Skipping..." 1>&2
[ -d "$docs_dir/man" ]       && stats_man       || echo "WARNING: $docs_dir/man not found. Skipping..." 1>&2
[ -d "$docs_dir/keys" ]      && stats_keys      || echo "WARNING: $docs_dir/keys not found. Skipping..." 1>&2
[ -d "$docs_dir/tutorials" ] && stats_tutorials || echo "WARNING: $docs_dir/tutorials not found. Skipping..." 1>&2
[ -d "$website_dir/" ]       && stats_website   || echo "WARNING: $website_dir/ not found. Skipping..." 1>&2

echo "
    </div>
  </body>
</html>
"

# -*- mode: sh; sh-basic-offset: 4; indent-tabs-mode: nil; -*-
# vim: set filetype=sh sw=4 sts=4 expandtab autoindent:
