Inkscape Documentation
======================

This includes all files required for creating localized documentation for Inkscape.

At the time of this writing this includes:
 - Inkscape man pages (*man*)
 - Inkscape keyboard and mouse reference (*keys*)
 - Inkscape tutorials (*tutorials*)

It also has rules to create translation statistics (*statistics*).

Translating Documentation in this repository
--------------------------------------------

**What needs translation?**

An overview about the status of translations in the Inkscape project is available at:

 - https://inkscape.org/doc/devel/translations-statistics-master.html (for Inkscape master branch)
 - https://inkscape.org/doc/devel/translations-statistics-092.html (for the 0.92.x series)

**How do I get the files?**

Translators can download the files from this repository as a zip archive and translate the documentation po files for their language.
At a later point, we may add info here about different versions that need to be translated.

For non-programmers, the quickest option to obtain the files is to download the [zip archive](https://gitlab.com/inkscape/inkscape-docs/documentation/repository/0.92.x/archive.zip).

If you feel comfortable using git and/or the gitlab infrastructure, you're welcome to make your own branch and/or fork.

**Which files need to be translated?**

You can find po files ready for translation in the following subdirectories:
 - keys
 - man
 - tutorials/\<various tutorial subdirectories\>

If there isn't a file for your language yet, you can create a new one by copying the files 'keys.pot', 'man.pot' or '\<tutorial_name.pot\>' and renaming the copy to '<your_language_code>.po'.

**Is there anything important I need to know?**

 - The translation files for the manual contain some unusual formatting, e.g. <pre>C\<E\<lt\>defsE\<gt\>\></pre> Please leave these strings intact.

**I'm ready. How do I submit my translations?**

To get your translations into this repository, please create a new bug report at https://bugs.launchpad.net/inkscape and attach your edited file.
You may add the tags 'documentation' and 'translation' to it.

Alternatively, if you're comfortable with the functionality gitlab and git provide, you can make a merge request for this repository once you are ready to share your work.


Creating documentation
----------------------

Each of the subdirectories has a README with detailed information on how to use (including software requirements).

Also you'll find a Makefile in each subdirectory for usage with GNU make (use `make help` for usage information)

If you're feeling lucky (or know that you have all requirements installed)
you can also use the convenience targets from the top level Makefile:
 - use `make` to generate all documentation
 - use `make help` to see additional usage information

*Note: As git does not track file modification times `make` might not be able to determine which targets need to be
 re-made after checking out new files. In order to re-make all files use `make --always-make` (or `make -B`).*